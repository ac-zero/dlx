library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.utils.all;

entity adder_comp is
	generic(reg_size: integer := 32);
	port(	A: in std_logic_vector(reg_size-1 downto 0);
		Co: in std_logic;
		isUnsigned: in std_logic;
		code: in std_logic_vector(2 downto 0);
		S: out std_logic);
end adder_comp;

architecture bhv of adder_comp is

	signal Z,MSB: std_logic;

	subtype tree_row_t is std_logic_vector(reg_size-1 downto 0);
	type tree_t is array ( 0 to log2(reg_size) ) of tree_row_t;
	signal tree: tree_t;	
begin

	--- CODE
	--- 001 => A == B?
	--- 010 => A != B?
	--- 011 => A < B?
	--- 100 => A <= B?
	--- 101 => A > B?
	--- 110 => A >= B?
	--- others => nop
	
	firstRowGen: for i in 0 to reg_size-1 generate
		tree(log2(reg_size))(i) <= A(i);
	end generate;

	nopTree: for i in log2(reg_size)-1 downto 0 generate
		norGen: if i mod 2 = 0 generate 
			rowGen: for j in 0 to 2**i-1 generate
				norGen: tree(i)(j) <= tree(i+1)(2*j) nor tree(i+1)(2*j+1);
			end generate;
		end generate norGen;
		nandGen: if i mod 2 /= 0 generate
			rowGen: for j in 0 to 2**i-1 generate
				norGen: tree(i)(j) <= tree(i+1)(2*j) nand tree(i+1)(2*j+1);
			end generate;
		end generate nandGen;
	end generate;

	Z <= tree(0)(0) and not(A(0));

	MSB <= A(reg_size -1);

	process(Z,Co,code,isUnsigned,MSB)

	begin
		case code is
			when "001" =>
				S <= Z;

			when "010" =>
				S <= not(Z);

			when "011" =>
				if isUnsigned = '1' then
					S <= not(Co);
				else 
					S <= MSB;
				end if;
			
			when "100" =>
				if isUnsigned = '1' then
					S <= (not Co) or Z;
				else 
					S <= MSB or Z;
				end if;
	
			when "101" =>				
				if isUnsigned = '1' then
					S <= (not Z) and Co;
				else 
					S <= not(MSB) and not(Z);
				end if;
			when "110" =>
				if isUnsigned = '1' then
					S <= Co;
				else 
					S <= not(MSB)	;
				end if;
			
			when others =>
				S <= '0';
			
		end case;
	end process;
end bhv;


