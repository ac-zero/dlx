library ieee; 
use ieee.std_logic_1164.all; 
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
--use WORK.constants.all;

entity csb is
	port(   Cin:	IN	std_logic;
		DATA_A: IN	std_logic_vector(3 downto 0);
		DATA_B: IN	std_logic_vector(3 downto 0);
		S:	OUT	std_logic_vector(3 downto 0));
end csb;

architecture STRUCTURAL of csb is


component RCA is
	generic (NBIT: integer:= 4);
	Port (	A:	In	std_logic_vector(NBIT-1 downto 0);
		B:	In	std_logic_vector(NBIT-1 downto 0);
		Ci:	In	std_logic;
		S:	Out	std_logic_vector(NBIT-1 downto 0);
		Co:	Out	std_logic);
end component;

component MUX21_GENERIC is
	generic (NBIT: integer:= 4);
	Port (	A:	In	std_logic_vector(NBIT-1 downto 0) ;
		B:	In	std_logic_vector(NBIT-1 downto 0);
		SEL:	In	std_logic;
		Y:	Out	std_logic_vector(NBIT-1 downto 0));
end component;


constant N: integer := 4;
signal RCA1_out: std_logic_vector(3 downto 0);
signal RCA2_out: std_logic_vector(3 downto 0);
signal Co1: std_logic;
signal Co2: std_logic;
signal Cin_0: std_logic;
signal Cin_1: std_logic;


begin


Cin_0 <= '0';
Cin_1 <= '1';

RCA1: RCA
	generic map(NBIT => N)
	port map(DATA_A, DATA_B, Cin_0, RCA1_out, Co1);

RCA2: RCA
	generic map(NBIT => N)
	port map(DATA_A, DATA_B, Cin_1, RCA2_out, Co2);

MUX: MUX21_GENERIC
	generic map(NBIT => N)
	port map(RCA1_out, RCA2_out, Cin, S);

end STRUCTURAL;

configuration CFG_CSB_STRUCTURAL of csb is
for STRUCTURAL
	for RCA1: RCA
		use configuration WORK.CFG_RCA_STRUCTURAL;
	end for;
	for RCA2: RCA
		use configuration WORK.CFG_RCA_STRUCTURAL;
	end for;
	for MUX: MUX21_GENERIC
		use configuration WORK.CFG_MUX21_GEN_STRUCTURAL;
	end for;
end for;
end CFG_CSB_STRUCTURAL;




















