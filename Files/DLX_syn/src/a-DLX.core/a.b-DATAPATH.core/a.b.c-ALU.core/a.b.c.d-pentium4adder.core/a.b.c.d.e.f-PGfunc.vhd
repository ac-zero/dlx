library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity PGfunc is
	port(
		gik, pik: in std_logic;
		gikp, pikp: in std_logic;
		gij, pij: out std_logic
	);
end PGfunc;

architecture Behavioral of PGfunc is

begin

	gij <= gik or (pik and gikp);
	pij <= pik and pikp;

end Behavioral;

