library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Mux8x1 is
	generic ( N: integer := 8 );
	port(
		A,B,C,D,E,F,G,H: in std_logic_vector(N-1 downto 0);
		S: in std_logic_vector(2 downto 0);
		O: out std_logic_vector(N-1 downto 0)		
	);
end Mux8x1;

architecture Behavioral of Mux8x1 is
begin
	O <= 	A when S = "000" else
			B when S = "001" else 
			C when S = "010" else
			D when S = "011" else
			E when S = "100" else
			F when S = "101" else
			G when S = "110" else H;
end Behavioral;

