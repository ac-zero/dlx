library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity BoothEnc is
	generic(R: integer:= 3; E: integer:= 3);
	port(
		A: in std_logic_vector(R-1 downto 0);
		O: out std_logic_vector(E-1 downto 0)
	);
end BoothEnc;

architecture Behavioral of BoothEnc is

begin
	O <= 	std_logic_vector(to_unsigned(1,R)) when A = std_logic_vector(to_unsigned(1,E)) else 
			std_logic_vector(to_unsigned(1,R)) when A = std_logic_vector(to_unsigned(2,R)) else
			std_logic_vector(to_unsigned(3,R)) when A = std_logic_vector(to_unsigned(3,R)) else
			std_logic_vector(to_unsigned(4,R)) when A = std_logic_vector(to_unsigned(4,R)) else
			std_logic_vector(to_unsigned(2,R)) when A = std_logic_vector(to_unsigned(5,R)) else
			std_logic_vector(to_unsigned(2,R)) when A = std_logic_vector(to_unsigned(6,R)) else (others => '0');

end Behavioral;

