library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Comp2 is
	generic(N: integer:= 8);
	port(
		A: in std_logic_vector(N-1 downto 0);
		O: out std_logic_vector(N-1 downto 0)
	);
end Comp2;

architecture Behavioral of Comp2 is

begin
	process(A)
		variable S: signed(N downto 0);
	begin
		S := signed(A) * to_signed(-1, 1);
		O <= std_logic_vector(S(N-1 downto 0));
	end process;

end Behavioral;

