library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.utils.all;

entity jump_unit is
	generic(reg_size: integer := 32);
	port(	A: in std_logic_vector(reg_size-1 downto 0);
		code: in std_logic_vector(2 downto 0);
		J: out std_logic);
end jump_unit;

architecture bhv of jump_unit is

	signal Z: std_logic;

	subtype tree_row_t is std_logic_vector(reg_size-1 downto 0);
	type tree_t is array ( 0 to log2(reg_size) ) of tree_row_t;
	signal tree: tree_t;	
begin

	--- CODE
	--- 000 => no jump
	--- 001 => BEQZ
	--- 010 => BNEZ
	--- 011 => no jump
	--- 100 => J
	--- 101 => JR
	--- 110 => JAL
	--- 111 => JALR


	--- J => if '0' no jump, if '1' jump
	
	firstRowGen: for i in 0 to reg_size-1 generate
		tree(log2(reg_size))(i) <= A(i);
	end generate;

	nopTree: for i in log2(reg_size)-1 downto 0 generate
		norGen: if i mod 2 = 0 generate 
			rowGen: for j in 0 to 2**i-1 generate
				norGen: tree(i)(j) <= tree(i+1)(2*j) nor tree(i+1)(2*j+1);
			end generate;
		end generate norGen;
		nandGen: if i mod 2 /= 0 generate
			rowGen: for j in 0 to 2**i-1 generate
				norGen: tree(i)(j) <= tree(i+1)(2*j) nand tree(i+1)(2*j+1);
			end generate;
		end generate nandGen;
	end generate;

	Z <= tree(0)(0) and not(A(0));

	process(Z,code)

	begin
		case code is
			when "001" =>
				J <= Z;

			when "010" =>
				J <= not(Z);
		
			when others =>
				J <= code(2);
			
		end case;
	end process;
end bhv;


