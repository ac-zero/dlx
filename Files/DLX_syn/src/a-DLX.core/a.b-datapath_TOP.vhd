library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.constants.all;
use work.utils.all;


entity datapath is
	generic(
		IR_SIZE            :     integer := IR_SIZE; -- Instruction Register Size
		DMEM_CW_SIZE       :     integer := DMEM_CW_SIZE --dmemCW Size    
	);
	port(
		Clk                : in  std_logic;  -- Clock
		Rst                : in  std_logic;  -- Reset:Active-Low
		sign			   : in  std_logic;  --to differentiate addi-addui
		-- IF Control Signal
		IR_LATCH_EN        : in std_logic;  -- Instruction Register Latch Enable
		NPC_LATCH_EN       : in std_logic;
														-- NextProgramCounter Register Latch Enable
		-- ID Control Signals
		RegA_LATCH_EN      : in std_logic;  -- Register A Latch Enable
		RegB_LATCH_EN      : in std_logic;  -- Register B Latch Enable
		RegIMM_LATCH_EN    : in std_logic;  -- Immediate Register Latch Enable
	
		-- EX Control Signals
		--MUXA_SEL           : in std_logic;  -- MUX-A Sel
		MUXB_SEL           : in std_logic;  -- MUX-B Sel
		ALU_OUTREG_EN      : in std_logic;  -- ALU Output Register Enable
		J_CODE             : in std_logic_vector(2 downto 0);	
		
		-- ALU Operation Code
		ALU_OPCODE         : in std_logic_vector(EXE_CONTR_SIZE-1 downto 0);

		-- MEM Control Signals
		DRAM_EN            : in std_logic;  -- Data RAM Enable
		DRAM_RD_WRN        : in std_logic;  -- Data RAM RD/WR
		LMD_LATCH_EN       : in std_logic;  -- LMD Register Latch Enable
		--PC_LATCH_EN        : in std_logic;  -- Program Counte Latch Enable
		DMEM_CW            : in std_logic_vector(DMEM_CW_SIZE-1 downto 0);  -- dmemCW vector
	
		-- WB Control signals
		WB_MUX_SEL         : in std_logic;  -- Write Back MUX Sel
		RF_WE_1              : in std_logic;  -- Register File Write 1 Enable
		RF_WE_2              : in std_logic;  -- Register File Write 2 Enable
		
		-- OUTPUTS
		IR_OUT		: out std_logic_vector(IR_SIZE-1 downto 0);

		-- FW SIGNALS: 
		FW_EXE_MEMN_A	    : in std_logic;
		FW_EXE_MEMN_B	    : in std_logic;
		FW_EN_A	      	    : in std_logic;
		FW_EN_B	       	    : in std_logic;
		FW_EN_M	       	    : in std_logic;
		
		-- HOLD SIGNALS
		hold_sel: in std_logic;  
		hold_en: in std_logic;
		
		FLUSH_EN: out std_logic;

		--syn imem
		addr_imem: out std_logic_vector(PC_size-1 downto 0);
		d_out_imem: in std_logic_vector(IR_size-1 downto 0);

		--syn (dmem)
		rm_dmem,wm_dmem,en_dmem: out std_logic;
		addr_dmem: out std_logic_vector(SYSTEM_DATA_SIZE-1 downto 0);
		d_in_dmem: out std_logic_vector(SYSTEM_DATA_SIZE-1 downto 0);
		dmemCW_dmem: out std_logic_vector(DMEM_CW_SIZE-1 downto 0);
		d_out_dmem: in std_logic_vector(SYSTEM_DATA_SIZE-1 downto 0) 
		    
		
	);
end datapath;

architecture Structural of datapath is


	component adder_gen is
		generic( N: integer := 8);
		port(	en: in std_logic;
			A: in std_logic_vector(N-1 downto 0);
			B: in std_logic_vector(N-1 downto 0);
			Z: out std_logic_vector(N-1 downto 0));
	end component;

	component ALU is
		generic (
			N: integer := N;
			ALU_CONTR_SIZE: integer:= ALU_CONTR_SIZE
		);
		port(
			A,B: in std_logic_vector(N-1 downto 0);
			contr: in std_logic_vector(ALU_CONTR_SIZE-1 downto 0);
			Co: out std_logic;
			S: out std_logic_vector(N-1 downto 0)
		);
	end component ALU;

	component BOOTHMUL is
		generic( N: integer := N_MUL; R: integer := R);
		port(
			A,B: in std_logic_vector(N-1 downto 0);
			S: out std_logic_vector(2*N-1 downto 0)
		);
	end component BOOTHMUL;

	component jump_unit is
		generic(reg_size: integer := 32);
		port(	A: in std_logic_vector(reg_size-1 downto 0);
			code: in std_logic_vector(2 downto 0);
			J: out std_logic);
	end component;

	component adder_comp is
		generic(reg_size: integer := 32);
		port(	A: in std_logic_vector(reg_size-1 downto 0);
			Co: in std_logic;
			isUnsigned: in std_logic;
			code: in std_logic_vector( 2 downto 0);
			S: out std_logic);
	end component;

	component reg_file is
		generic(reg_size: integer := 32);
		port(	enable: in std_logic;
			clk: in std_logic;
			rst: in std_logic;
			rd1_en: in std_logic;
			rd2_en: in std_logic;
			wr_en_1, wr_en_2: in std_logic;
			rd1_addr: in std_logic_vector(log2(reg_size)-1 downto 0);
			rd2_addr: in std_logic_vector(log2(reg_size)-1 downto 0);
			wr_addr:  in std_logic_vector(log2(reg_size)-1 downto 0);
			data_in_1:  in std_logic_vector(reg_size-1 downto 0);
			data_in_2:  in std_logic_vector(reg_size-1 downto 0);
			data_out_1: out std_logic_vector(reg_size-1 downto 0);
			data_out_2: out std_logic_vector(reg_size-1 downto 0));
	end component;

	component reg_gen is
		generic(NBIT: integer := 8);
		Port (	EN:	In 	std_logic;
			D:	In	std_logic_vector(NBIT-1 downto 0);
			CK:	In	std_logic;
			RESET:	In	std_logic;
			Q:	Out	std_logic_vector(NBIT-1 downto 0));
	end component;

	component reg_gen_ddr is
		generic(NBIT: integer := 8);
		Port (	EN:	In 	std_logic;
			D:	In	std_logic_vector(NBIT-1 downto 0);
			CK:	In	std_logic;
			RESET:	In	std_logic;
			Q:	Out	std_logic_vector(NBIT-1 downto 0));
	end component;

	component FF is
		Port (	EN:	In	std_logic;
			D:	In	std_logic;
			CK:	In	std_logic;
			RESET:	In	std_logic;
			Q:	Out	std_logic);
	end component;


	component sign_ext is
		port(sign: in std_logic;
			A : in std_logic_vector(15 downto 0);
			Z : out std_logic_vector(31 downto 0));
	end component;

	component mux21_gen is
		Generic (NBIT: integer:= 32);
		Port (	A:	In	std_logic_vector(NBIT-1 downto 0) ;
			B:	In	std_logic_vector(NBIT-1 downto 0);
			SEL:	In	std_logic;
			Y:	Out	std_logic_vector(NBIT-1 downto 0));
	end component;

	signal nclk: std_logic;
	signal opcode: std_logic_vector( OP_CODE_SIZE-1 downto 0);
	signal PC_OUT, NPC_OUT, NPC_IN, mux_pc_out, add_out, add_b_out: std_logic_vector(PC_size-1 downto 0) := (others => '0');
	signal IR_OUT_REG, imem_dout, signExt_out: std_logic_vector(IR_size-1 downto 0):= (others => '0');
	signal RF_EN, LMD_en : std_logic:= '0';
	signal data_in_1, data_in_2, LMD_out, dMem_out: std_logic_vector(reg_size-1 downto 0):= (others => '0');
	signal mux_wb_out: std_logic_vector(Nbit*2-1 downto 0);
	signal aluReg_out: std_logic_vector(Nbit*2-1 downto 0);

	signal data_a_reg_out, data_b_reg_out, dmem_data_in: std_logic_vector(reg_size-1 downto 0);
	signal add_en, mux_jr_sel: std_logic;

	subtype pipe_reg_wra_out_t is std_logic_vector(OPR_SIZE-1 downto 0);
	type pipe_reg_wra_out_array_t is array ( 0 to 3 ) of pipe_reg_wra_out_t;
	signal pipe_reg_wra_out: pipe_reg_wra_out_array_t;

	subtype pipe_reg_npc_out_t is std_logic_vector(PC_SIZE-1 downto 0);
	type pipe_reg_npc_out_array_t is array ( 0 to 3 ) of pipe_reg_npc_out_t;
	signal pipe_reg_npc_out: pipe_reg_npc_out_array_t;

	subtype pipe_reg_ju_out_t is std_logic_vector(2 downto 0);
	type pipe_reg_ju_out_array_t is array ( 0 to 3 ) of pipe_reg_ju_out_t;
	signal pipe_reg_ju_out: pipe_reg_ju_out_array_t;

	signal IMM_en,zComp_en, jComp_out, aluReg_en : std_logic:= '0';
	signal imm_reg_out, data_A_out, data_B_out: std_logic_vector(IR_size-1 downto 0):= (others => '0');

	signal mux_fw_exe_src_a_out,mux_fw_exe_src_b_out: std_logic_vector(reg_size-1 downto 0);
	signal mux_fw_exe_a_out,mux_fw_exe_b_out: std_logic_vector(reg_size-1 downto 0);
	signal wb_reg_out: std_logic_vector(2*reg_size-1 downto 0);
	signal mux_fw_exe_src_m_out: std_logic_vector(reg_size-1 downto 0);

	signal RF_ADDR_A, RF_ADDR_B:std_logic_vector(log2(reg_size)-1 downto 0):= (others => '0');
	signal RTYPEorITYPE_IMM: std_logic_vector(imm_size-1 downto 0):= (others => '0');
	signal data_mem_wm: std_logic:= '0';

	-- ALU signals
	signal ALU_in_2: std_logic_vector(reg_size-1 downto 0):= (others => '0');
	signal ALU_out, ALU_reg_in: std_logic_vector(reg_size-1 downto 0):= (others => '0');
	signal ALU_pipe: std_logic_vector(Nbit*2-1 downto 0);
	signal ALU_co, addComp_out: std_logic;
	signal ALU_opc, COMP_opc, ALU_DEF_OPC, COMP_DEF_OPC: std_logic_vector(ALU_CONTR_SIZE-1 downto 0);

	--MUL signals
	signal mul_out: std_logic_vector(Nbit*2-1 downto 0);	
	signal ALU_MUL_REG_IN: std_logic_vector(Nbit*2-1 downto 0);
	signal mulSign: std_logic;
	signal muxSel: std_logic;
	signal muxMUL_out_A, muxMUL_out_B, MUL_SIGN_A, MUL_SIGN_B: std_logic_vector(N_MUL-Nbit-1 downto 0);
	signal muxMUL_sel: std_logic;
	signal void: std_logic_vector(3 downto 0);

	-- WB signals
	signal readReg1, jumpType1, link2, jumpType2, mux_wr_addr_sel: std_logic;
	signal mux_wra_out: std_logic_vector(OPR_SIZE -1 downto 0);

	-- HOLD signals
	signal mux_hold_out,hold_out: std_logic_vector(PC_SIZE -1 downto 0);

	-- utils
	signal zero_sig: std_logic;
	signal four: std_logic_vector(PC_size-1 downto 0) := std_logic_vector(to_unsigned(4,PC_SIZE));
	signal alu_comp_out: std_logic_vector(reg_size-1 downto 0);
	signal npc_padding: std_logic_vector(reg_size*2-PC_SIZE-1 downto 0);

	--synthesis signals
	signal temp_sig_B: std_logic_vector(reg_size*2-1 downto 0);
	signal all_zeros: std_logic_vector(Nbit-1 downto 0);
	signal data_in_1_2_total: std_logic_vector(reg_size*2-1 downto 0);
	signal zeros_alu_reg_in_total: std_logic_vector(reg_size*2-1 downto 0);
	signal zeros_dMem_out_total: std_logic_vector(reg_size*2-1 downto 0);

	begin

	--synthesis signals
	all_zeros <= (others => '0');
	--data_in_1_2_total <= data_in_2 & data_in_1;
	zeros_alu_reg_in_total <= all_zeros & ALU_reg_in;
	zeros_dMem_out_total <= all_zeros & dMem_out;

	nclk <= not(clk);
						
	muxSel <= '1' when ALU_OPCODE(EXE_CONTR_SIZE-1 downto EXE_CONTR_SIZE-2)="01" else '0';
	mulSign <= '1' when to_integer(unsigned(ALU_OPCODE))=9 else '0';

	muxMUL_sel <= '0' when ALU_OPCODE="01000" else '1';
	MUL_SIGN_A <= data_a_reg_out(Nbit-1)&data_a_reg_out(Nbit-1);
	MUL_SIGN_B <= data_b_reg_out(Nbit-1)&data_b_reg_out(Nbit-1);

	opcode <= IR_OUT_REG(IR_size-1 downto IR_size-OP_CODE_SIZE);

	FLUSH_EN <= jComp_out;
	

	zero_sig <= '0';
	alu_comp_out(reg_size-1 downto 1) <= (others => '0');
	add_en <= not(rst);

	data_mem_wm <= not(DRAM_RD_WRN);
	RF_EN <= RegA_LATCH_EN or RegB_LATCH_EN;
	IR_OUT <= imem_dout;

	npc_padding <= (others => '0');
	ALU_DEF_OPC <= "001";
	COMP_DEF_OPC <= "000";

-- FETCH

	PC_32: reg_gen
		generic map(NBIT => PC_SIZE)
		port map(EN => IR_LATCH_EN, D => mux_hold_out, CK => clk, RESET => rst, Q => PC_OUT);
	
	--syn (imem)
	addr_imem <= PC_OUT;
	imem_dout <= d_out_imem;

	IR: reg_gen
		generic map(NBIT => IR_SIZE)
		port map(EN => IR_LATCH_EN, D => imem_dout, CK => clk, RESET => rst, Q => IR_OUT_REG);	
	
	add4: adder_gen
		generic map(N => PC_size)
		port map(en => add_en, A => PC_OUT, B => four, Z => add_out);

	mux_PC: mux21_gen
		generic map(NBIT => PC_SIZE)
		port map(A => add_out, B => add_b_out, SEL => jComp_out, Y => mux_pc_out);		

	NPC_32: reg_gen
		generic map(NBIT => PC_SIZE)
		port map(EN => IR_LATCH_EN, D => NPC_IN, CK => clk, RESET => rst, Q => NPC_OUT);

-- DECODE

	-- This process extract the addresses from IR
	IRproc: process(opcode,IR_OUT_REG)
	begin
		case opcode(OP_CODE_SIZE-1 downto 1) is
			-- F or R Type
			when "00000" =>
				-- RF Address A  
				RF_ADDR_A <=  IR_OUT_REG(IR_size-OP_CODE_SIZE-1 downto IR_size-OP_CODE_SIZE-opr_size);
				-- RF Address B  	
				RF_ADDR_B <= IR_OUT_REG(IR_size-OP_CODE_SIZE-opr_size-1 downto IR_size-OP_CODE_SIZE-2*opr_size);
				-- WR Address
				pipe_reg_wra_out(0) <= IR_OUT_REG(IR_size-OP_CODE_SIZE-opr_size*2-1 downto func_size);
				-- IMM
				RTYPEorITYPE_IMM <= (others => '0') ;

			-- I_TYPE
			when others =>
				-- RF Address A  
				RF_ADDR_A <= IR_OUT_REG(IR_size-OP_CODE_SIZE-1 downto IR_size-OP_CODE_SIZE-OPR_SIZE);
				-- FOR STORE INSTR: RD_B_ADDR IS EXCHANGED WITH WR_ADDR
				if opcode(OP_CODE_SIZE-1 downto 3) = "101" then
			
					-- RF Address B
					RF_ADDR_B <= IR_OUT_REG(IR_size-OP_CODE_SIZE-opr_size-1 downto IR_size-OP_CODE_SIZE-2*opr_size);
					-- WR Address
					pipe_reg_wra_out(0) <= (others => 'Z');
				else 
					-- RF Address B
					RF_ADDR_B <= ( others => 'Z' );

					-- WR Address
					pipe_reg_wra_out(0) <= IR_OUT_REG(IR_size-OP_CODE_SIZE-opr_size-1 downto IR_size-OP_CODE_SIZE-2*opr_size);
				end if;

				-- IMM
				RTYPEorITYPE_IMM <= IR_OUT_REG(IR_size-OP_CODE_SIZE-2*opr_size-1 downto 0);
		end case;
	

	end process;
	

	PIPE_REG_WRA_GEN: for i in 0 to 2 generate
		reg_wra: reg_gen
			generic map(NBIT => OPR_SIZE)
			port map(EN => '1', D => pipe_reg_wra_out(i), CK => nclk, RESET => rst, Q => pipe_reg_wra_out(i+1));
	end generate;

	pipe_reg_npc_out(0) <= add_out	;

	PIPE_REG_NPC_GEN: for i in 0 to 2 generate
		reg_wra: reg_gen
			generic map(NBIT => PC_SIZE)
			port map(EN => '1', D => pipe_reg_npc_out(i), CK => nclk, RESET => rst, Q => pipe_reg_npc_out(i+1));
	end generate;
	

	-- J CODE:
	-- [2] BasicJumps/AdvJumps - 0/1
	-- [1] Code1/Link (Basic/Adv)
	-- [0] Code2/ReadReg (Basic/Adv)

	-- for DEC stage
	jumpType1 <= J_CODE(2);
	readReg1 <= J_CODE(0);

	pipe_reg_ju_out(0)(2) <= J_CODE(2);
	pipe_reg_ju_out(0)(1) <= J_CODE(1);
	pipe_reg_ju_out(0)(0) <= J_CODE(0);
	
	-- For WB stage
	jumpType2 <= pipe_reg_ju_out(3)(2);
	link2 <= pipe_reg_ju_out(3)(1);
	
	PIPE_REG_JU_GEN: for i in 0 to 2 generate
		reg_wra: reg_gen
			generic map(NBIT => 3)
			port map(EN => '1', D => pipe_reg_ju_out(i) , CK => nclk, RESET => rst, Q => pipe_reg_ju_out(i+1));
	end generate;

	mux_wr_addr_sel <= jumpType2 and link2;  -- AdvJump AND link

	mux_wr_addr: mux21_gen
		generic map(NBIT => OPR_SIZE)
		port map(A =>  pipe_reg_wra_out(3), B => "11111", SEL => mux_wr_addr_sel, Y => mux_wra_out);
	mux_jr_sel <= jumpType1 and readReg1;

	RF: reg_file
		generic map(reg_size => reg_size)
		port map(enable => '1', 
			clk => nclk,
			rst => rst,	
			rd1_en => RegA_LATCH_EN,
			rd2_en => RegB_LATCH_EN, 
			wr_en_1 => RF_WE_1,
			wr_en_2 => RF_WE_2,
			rd1_addr => RF_ADDR_A,
			rd2_addr => RF_ADDR_B,
			wr_addr => mux_wra_out,
			data_in_1 => data_in_1_2_total(reg_size-1 downto 0),
			data_in_2 => data_in_1_2_total(2*reg_size-1 downto reg_size), 
			data_out_1 => data_A_out, 
			data_out_2 => data_B_out
		);

	mux_FW_EXE_SRC_A: mux21_gen
		generic map(NBIT => reg_size)
		port map(A => mux_wb_out(reg_size-1 downto 0), B => ALU_out, SEL => FW_EXE_MEMN_A, Y => mux_fw_exe_src_A_out);
	
	mux_FW_EXE_SRC_B: mux21_gen
		generic map(NBIT => reg_size)
		port map(A => mux_wb_out(reg_size-1 downto 0), B => ALU_out, SEL => FW_EXE_MEMN_B, Y => mux_fw_exe_src_B_out);
	
	mux_FW_EXE_A: mux21_gen
		generic map(NBIT => reg_size)
		port map(A => data_A_out, B => mux_fw_exe_src_a_out, SEL => FW_EN_A, Y => mux_fw_exe_a_out);

	mux_FW_EXE_B: mux21_gen
		generic map(NBIT => reg_size)
		port map(A => data_B_out, B => mux_fw_exe_src_b_out, SEL => FW_EN_B, Y => mux_fw_exe_b_out);

	DATA_A_REG: reg_gen
		generic map(NBIT => IR_SIZE)
		port map(EN => '1', D => mux_fw_exe_a_out, CK => clk, RESET => rst, Q => data_a_reg_out);	
	
	DATA_B_REG: reg_gen
		generic map(NBIT => IR_SIZE)
		port map(EN => '1', D => mux_fw_exe_b_out, CK => clk, RESET => rst, Q => data_b_reg_out);	
	

	signExt: sign_ext
		port map(sign => sign, A => RTYPEorITYPE_IMM, Z => signExt_out);

	IMM: reg_gen
		generic map(NBIT => IR_size)
		port map(EN => RegIMM_LATCH_EN, D => signExt_out, CK => clk, RESET => rst, Q => imm_reg_out);

	adder_branch: adder_gen
		generic map(N => PC_size)
		port map(en => add_en, A => signExt_out(PC_SIZE-1 downto 0), B => NPC_OUT, Z => add_b_out);

	--HOLD: USED TO HOLD THE VALUE IN CASE OF BRANCH AND STALL OF INSTR IN THE B. SLOT
	HOLD_REG: reg_gen
		generic map(NBIT => PC_SIZE)
		port map(EN => hold_EN, D => NPC_IN, CK => clk, RESET => rst, Q => hold_out);
	

	mux_HOLD: mux21_gen
		generic map(NBIT => PC_SIZE)
		port map(A => NPC_IN, B => hold_out, SEL => hold_sel, Y => mux_hold_out);
 	--

	mux_PC_jr: mux21_gen
		generic map(NBIT => PC_SIZE)
		port map(A => mux_pc_out, B => mux_fw_exe_a_out(PC_SIZE-1 downto 0), SEL => mux_jr_sel, Y => NPC_IN);

	jumpUnit: jump_unit
		generic map(reg_size => reg_size)
		port map(A => mux_fw_exe_a_out, code => J_CODE , J => jComp_out);

	-- EXEC
		
	mux_B: mux21_gen
		generic map(NBIT => reg_size)
		port map(A => data_b_reg_out, B => imm_reg_out, SEL => MUXB_SEL, Y => ALU_in_2);

	mux_ALU_OPCODE: mux21_gen
		-- Sel: '0' => OP_CODE, '1' => SUB_CODE (for comp ops)
		generic map(NBIT => ALU_CONTR_SIZE)
		port map(A => ALU_OPCODE(ALU_CONTR_SIZE-1 downto 0), B => ALU_DEF_OPC, SEL => ALU_OPCODE(EXE_CONTR_SIZE-1), Y => ALU_opc);


	alu_0: alu generic map (N => reg_size)
		port map (A => data_a_reg_out, B => ALU_in_2, contr => ALU_opc, S => ALU_out, Co => ALU_co);
	
	
	mux_COMP_OPCODE: mux21_gen
		-- Sel: '0' => NOP_CODE, '1' => OP_CODE
		generic map(NBIT => ALU_CONTR_SIZE)
		port map(A => COMP_DEF_OPC, B => ALU_OPCODE(ALU_CONTR_SIZE-1 downto 0), SEL => ALU_OPCODE(EXE_CONTR_SIZE-1), Y => COMP_opc);


	alu_comp: adder_comp generic map (reg_size => reg_size)
		port map(A => ALU_out, isUnsigned => ALU_OPCODE(EXE_CONTR_SIZE-2), Co => ALU_co, code =>COMP_opc, S => addComp_out);
	
	alu_comp_out(0) <= addComp_out;

	mux_ALU_OUT: mux21_gen
		generic map(NBIT => reg_size)
		port map(A => ALU_out, B => alu_comp_out, SEL => ALU_OPCODE(EXE_CONTR_SIZE-1), Y => ALU_reg_in);

	mul: BOOTHMUL
		generic map(N => N_MUL, R => R)
		port map(A(N_MUL-1 downto N_MUL-2) => muxMUL_out_A, 
			A(N_MUL-3 downto 0) => data_a_reg_out,
			B(N_MUL-1 downto N_MUL-2) => muxMUL_out_B,
			B(N_MUL-3 downto 0) => data_b_reg_out,
			S(N_MUL*2-1 downto N_MUL*2-4) => void,
			S(Nbit*2-1 downto 0) => mul_out);

	muxMUL_A: mux21_gen
		generic map(NBIT => N_MUL-Nbit)
		port map(A => "00", B => MUL_SIGN_A, SEL => muxMUL_sel, Y => muxMUL_out_A);

	muxMUL_B: mux21_gen
		generic map(NBIT => N_MUL-Nbit)
		port map(A => "00", B => MUL_SIGN_B, SEL => muxMUL_sel, Y => muxMUL_out_B);


	mux_ALU_MUL: mux21_gen
		generic map(NBIT => Nbit*2)
		port map(A => zeros_alu_reg_in_total,
				B => mul_out, SEL => muxSel, Y => ALU_MUL_REG_IN);	
	
	aluReg: reg_gen
		generic map(NBIT => IR_size*2)
		port map(EN => ALU_OUTREG_EN, D => ALU_MUL_REG_IN, CK => clk, RESET => rst, Q => ALU_pipe);

	mux_FW_MEM_SRC: mux21_gen
		generic map(NBIT => reg_size)
		port map(A => data_b_reg_out, B => dMem_out, SEL => FW_EN_M, Y => mux_fw_exe_src_M_out);

	-- MEM


	DATA_IN_REG: reg_gen
		generic map(NBIT => reg_size)
		port map(EN => '1', D => mux_fw_exe_src_M_out, CK => clk, RESET => rst, Q => dmem_data_in);

	--syn (dmem)
		rm_dmem <= DRAM_RD_WRN;
		wm_dmem <= data_mem_wm;
		en_dmem <= DRAM_EN;
		addr_dmem <= ALU_pipe(Nbit-1 downto 0);
		d_in_dmem <= dmem_data_in;
		dmemCW_dmem <= DMEM_CW;
		dMem_out <= d_out_dmem;

	aluPipeReg: reg_gen
		generic map(NBIT => IR_size*2)
		port map(EN => '1', D => ALU_pipe, CK => nclk, RESET => rst, Q => aluReg_out);

	mux_MEM_stage_OUT: mux21_gen
		generic map(NBIT => reg_size*2)
		port map(A => zeros_dMem_out_total, B => ALU_pipe, SEL => WB_MUX_SEL, Y => mux_wb_out);
	
	-- WB
	mux_WB_link: mux21_gen
		generic map(NBIT => reg_size*2)
		port map(A => mux_wb_out, B => temp_sig_B,
			SEL => mux_wr_addr_sel , Y => data_in_1_2_total); 

			temp_sig_B(reg_size*2-1 downto PC_SIZE) <= npc_padding ;
			temp_sig_B(PC_SIZE-1 downto 0) <= pipe_reg_npc_out(3)(PC_SIZE-1 downto 0);


end Structural;

