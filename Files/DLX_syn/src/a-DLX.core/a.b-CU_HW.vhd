library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.constants.all;


entity dlx_cu is
  generic (
    MICROCODE_MEM_SIZE :     integer := MICROCODE_MEM_SIZE;  -- Microcode Memory Size
    FUNC_SIZE          :     integer := FUNC_SIZE;  -- Func Field Size for R-Type Ops
    OP_CODE_SIZE       :     integer := OP_CODE_SIZE;  -- Op Code Size
    ALU_OPC_SIZE       :     integer := EXE_CONTR_SIZE;  -- ALU Op Code Word Size
    IR_SIZE            :     integer := IR_SIZE;  -- Instruction Register Size    
    CW_SIZE            :     integer := CW_SIZE; -- Control Word Size
    DMEM_CW_SIZE       :     integer := DMEM_CW_SIZE;
    FORWARDING_ENABLED :     boolean := FALSE;
    BRANCH_DELAY_ENABLED:    boolean := FALSE
	); --dmemCW Size 
  port (
    Clk                : in  std_logic;  -- Clock
    Rst                : in  std_logic;  -- Reset
	sign			   : out  std_logic;  --to differentiate addi-addui
    -- Instruction Register
    IR_IN              : in  std_logic_vector(IR_SIZE - 1 downto 0);
    
    -- IF Control Signal
    IR_LATCH_EN        : out std_logic;  -- Instruction Register Latch Enable
    NPC_LATCH_EN       : out std_logic;
                                        -- NextProgramCounter Register Latch Enable
    -- ID Control Signals
    RegA_LATCH_EN      : out std_logic;  -- Register A Latch Enable
    RegB_LATCH_EN      : out std_logic;  -- Register B Latch Enable
    RegIMM_LATCH_EN    : out std_logic;  -- Immediate Register Latch Enable
    J_CODE             : out std_logic_vector(2 downto 0);

    -- EX Control Signals
    MUXB_SEL           : out std_logic;  -- MUX-B Sel
    ALU_OUTREG_EN      : out std_logic;  -- ALU Output Register Enable
    
    -- ALU Operation Code
    ALU_OPCODE         : out std_logic_vector(ALU_OPC_SIZE -1 downto 0);
   
    -- MEM Control Signals
    DRAM_EN            : out std_logic;  -- Data RAM Write Enable
    DRAM_RD_WRN        : out std_logic;  -- Data RAM Write Enable
    LMD_LATCH_EN       : out std_logic;  -- LMD Register Latch Enable
    PC_LATCH_EN        : out std_logic;  -- Program Counte Latch Enable
    DMEM_CW	       : out std_logic_vector(DMEM_CW_SIZE-1 downto 0); --dmemCW vector


    -- WB Control signals
    WB_MUX_SEL         : out std_logic;  -- Write Back MUX Sel
    RF_WE_1            : out std_logic;	-- Register File Write Enable 1
    RF_WE_2	       : out std_logic;  -- Register File Write Enable 2

    -- FW SIGNALS: 
    FW_EXE_MEMN_A      : out std_logic;
    FW_EXE_MEMN_B      : out std_logic;
    FW_EN_A	       : out std_logic;
    FW_EN_B	       : out std_logic;
    FW_EN_M	       : out std_logic;

    -- HOLD SIGNALS
    hold_sel	       : out std_logic;  
    hold_en	       : out std_logic;
   
    FLUSH_EN	       : in std_logic);
    
end dlx_cu;

architecture dlx_cu_hw of dlx_cu is
	type mem_array is array (integer range 0 to MICROCODE_MEM_SIZE - 1) of std_logic_vector(CW_SIZE - 1 downto 0);

-- IR_EN, NPC_EN, REGA_EN, REGB_EN, REGI_EN, Jcode2, Jcode1, Jcode0, MUXB, ALU-EN, DRAM_EN, DRAM_RD-WN, LMD_EN, PC_EN, DMEM_CW(), WB_MUX, RF_WE1, RF_WE2

-- EQ_COND: if it's '1' => branch if reg == 0
-- MUX_B: '0' for DATA_B, '1' for IMM
-- WB_MUX '0' from MEM, '1' from ALU
  signal cw_mem : mem_array := ("11110000010001000110", -- R type
                                "11110000010001000111", -- F type
                                "11001100100000000000", -- J (0X02) 
                                "11001110100001000110", -- JAL TBF
                                "11101001100000000000", -- BEQZ 
                                "11101010100000000000", -- BNEZ
                                "00000000000000000000", -- TBF
                                "00000000000000000000", -- TBF

                                "11101000110001000110", -- ADDI
                                "11101000110001000110", -- ADDUI
                                "11101000110001000110", -- SUBI
                                "11101000110001000110", -- SUBUI
                                "11101000110001000110", -- ANDI 
                                "11101000110001000110", -- ORI 
                                "11101000110001000110", -- XORI
                                "00000000000000000000", -- 

                                "00000000000000000000", -- 
                                "00000000000000000000",
                                "11101101100000000000", -- JR
                                "11101111100001000110", -- JALR
                                "11101000110001000110", -- SLLI 
                                "11000000000000000000", -- NOP 
                                "11101000110001000110", -- SRLI
                                "11101000110001000110", -- SRAI

                                "11101000110001000110", -- SEQI
                                "11101000110001000110", -- SNEI
                                "11101000110001000110", -- SLTI
                                "11101000110001000110", -- SGTI
                                "11101000110001000110", -- SLEI
                                "11101000110001000110", -- SGEI
                                "00000000000000000000", -- 
                                "00000000000000000000", --

				"11111000111111011010", -- LB
                                "11111000111111001010", -- LH
                                "00000000000000000000", -- 
                                "11111000111111101010", -- LW 
                                "11111000111111010010", -- LBU
                                "11111000111111000010", -- LHU 
                                "00000000000000000000", -- LF
                                "00000000000000000000", -- LD

                                "11111000111011011000", -- SB
                                "11111000111011001000", -- SH
                                "00000000000000000000", -- 
                                "11111000111011101000", -- SW
                                "00000000000000000000", -- 
                                "00000000000000000000", -- 
                                "00000000000000000000", -- SF
                                "00000000000000000000", -- SD

                                "00000000000000000000", -- 
                                "00000000000000000000", -- 
                                "00000000000000000000", -- 
                                "00000000000000000000", -- 
                                "00000000000000000000", -- 
                                "00000000000000000000", -- 
                                "00000000000000000000", -- 
                                "00000000000000000000", -- 

                                "00000000000000000000", -- ITLB
                                "00000000000000000000", -- 
                                "11101000110001000110", -- SLTUI
                                "11101000110001000110", -- SGTUI
                                "11101000110001000110", -- SLEUI
                                "11101000110001000110", -- SGEUI
                                "00000000000000000000", --  
                                "00000000000000000000");-- 
				-- to be completed (filled)
 

  component hazard_det is
	generic(I_size: integer := IR_SIZE;
		FORWARDING_ENABLED :     boolean := FALSE;
		BRANCH_DELAY_ENABLED:    boolean := FALSE
		);
	port(	clk,rst: in std_logic;
		IR_IN: in std_logic_vector(IR_SIZE-1 downto 0);
		stall: out std_logic;
		hold_sel: out std_logic;
		hold_en: out std_logic);
  end component;

  component forwarding is
	generic(I_size: integer := IR_SIZE);
	port(	clk,rst: in std_logic;
		IR_IN: in std_logic_vector(IR_SIZE-1 downto 0);
		stall: in std_logic;
		FW_EXE_MEMN_A: out std_logic;
		FW_EXE_MEMN_B: out std_logic;
		FW_EN_A: out std_logic;
		FW_EN_B: out std_logic;
		FW_EN_M: out std_logic
	);
  end component;

  signal stall: std_logic := '0';
  signal nclk: std_logic;

  signal flush,b_delay_enabled: std_logic;

  signal IR_opcode : std_logic_vector(OP_CODE_SIZE -1 downto 0);  -- OpCode part of IR
  signal IR_func : std_logic_vector(FUNC_SIZE-1 downto 0);   -- Func part of IR when Rtype

  -- control word is shifted to the correct stage
  signal cw1 : std_logic_vector(CW_SIZE -1 downto 0); -- first stage
  signal cw2 : std_logic_vector(CW_SIZE - 1 - 2 downto 0); -- second stage
  signal cw3 : std_logic_vector(CW_SIZE - 1 - 8 downto 0); -- third stage
  signal cw4 : std_logic_vector(CW_SIZE - 1 - 10 downto 0); -- fourth stage
  signal cw5 : std_logic_vector(CW_SIZE -1 - 18 downto 0); -- fifth stage

  -- so alu opcode
  signal aluop1: std_logic_vector(ALU_OPC_SIZE - 1 downto 0);	
  signal aluOpCode_i: std_logic_vector(ALU_OPC_SIZE-1 downto 0);

begin
  nclk <= not(clk);

  flushGen_0: if BRANCH_DELAY_ENABLED = false generate
	b_delay_enabled <= '0';
  end generate flushGen_0;
  flushGen_1: if BRANCH_DELAY_ENABLED = true generate
	b_delay_enabled <= '1';
  end generate flushGen_1;
 
  flush <= FLUSH_EN and not(b_delay_enabled);
 
  cw1 <= cw_mem(to_integer(unsigned(IR_opcode))) when rst = '0' and flush = '0' 
	 else (others => '0');
      
  IR_opcode(5 downto 0) <= IR_IN(31 downto 26);
  IR_func  <= IR_IN(FUNC_SIZE - 1 downto 0);

  -- HAZARD DETECTION UNIT	
  hazard_det_unit: hazard_det generic map(I_size => IR_SIZE,
		FORWARDING_ENABLED => FORWARDING_ENABLED,
		BRANCH_DELAY_ENABLED => BRANCH_DELAY_ENABLED)
	port map(clk => clk,
		rst => rst,
		IR_IN => IR_IN,
		stall => stall,
		hold_sel => hold_sel,
		hold_en => hold_en	);
  
  
  fwu_gen_1: if FORWARDING_ENABLED = TRUE generate
  -- FORWARDING UNIT
  	forwarding_unit: forwarding generic map (I_size => IR_SIZE)
		port map (
			clk => nclk,
			rst => rst,
			IR_IN => IR_IN,
			stall => stall,
			FW_EXE_MEMN_A => FW_EXE_MEMN_A,
			FW_EXE_MEMN_B => FW_EXE_MEMN_B,
			FW_EN_A => FW_EN_A,
			FW_EN_B => FW_EN_B,
			FW_EN_M => FW_EN_M	
		);
   	end generate;

  fwu_gen_0: if FORWARDING_ENABLED = FALSE generate
		FW_EXE_MEMN_A <= '0';
		FW_EXE_MEMN_B <= '0';
		FW_EN_A <= '0';
		FW_EN_B <= '0';
		FW_EN_M <= '0';
	end generate;



  -- stage one control signals
  IR_LATCH_EN  <= '1' when stall = '0' else '0';
  NPC_LATCH_EN <= '1' when stall = '0' else '0';
  
  -- stage two control signals
  RegA_LATCH_EN   <= cw2(CW_SIZE - 3);
  RegB_LATCH_EN   <= cw2(CW_SIZE - 4);
  RegIMM_LATCH_EN <= cw2(CW_SIZE - 5);
  J_CODE(2)       <= cw2(CW_SIZE - 6);
  J_CODE(1)       <= cw2(CW_SIZE - 7);
  J_CODE(0)  	  <= cw2(CW_SIZE - 8);

  -- stage three control signals
  MUXB_SEL      <= cw3(CW_SIZE - 9); 
  ALU_OUTREG_EN <= cw3(CW_SIZE - 10);
  
  -- stage four control signals
  DRAM_EN      <= cw4(CW_SIZE - 11);
  DRAM_RD_WRN  <= cw4(CW_SIZE - 12);
  LMD_LATCH_EN <= cw4(CW_SIZE - 13);
  PC_LATCH_EN  <= cw4(CW_SIZE - 14);
  DMEM_CW(DMEM_CW_SIZE-1) <= cw4(CW_SIZE - 15);
  DMEM_CW(DMEM_CW_SIZE-2) <= cw4(CW_SIZE - 16);
  DMEM_CW(DMEM_CW_SIZE-3) <= cw4(CW_SIZE - 17);
  WB_MUX_SEL <= cw4(CW_SIZE - 18);

  -- stage five control signals
  RF_WE_1      <= cw5(CW_SIZE - 19);
  RF_WE_2      <= cw5(CW_SIZE - 20);


  -- process to pipeline control words
  CW_PIPE: process (Clk, Rst)
  begin  -- process Clk
    if Rst = '1' then                   -- asynchronous reset (active low)
      cw2 <= (others => '0');
      cw3 <= (others => '0');
      cw4 <= (others => '0');
      cw5 <= (others => '0');	
      aluop1 <= (others => '0');	
      ALU_OPCODE <= (others => '0');

    elsif (rising_edge(Clk) or falling_edge(Clk)) then
	if Clk = '1' then  -- rising clock edge
	      	if stall = '1' then 
			cw2 <= (others => '0');		
		else
			cw2 <= cw1(CW_SIZE - 1 - 2 downto 0);
		end if;
		cw3 <= cw2(CW_SIZE - 1 - 8 downto 0); 	
		cw4 <= cw3(CW_SIZE - 1 - 10 downto 0);
		aluop1 <= aluOpCode_i;
		ALU_OPCODE <= aluop1;
		if IR_opcode="001001" then		--sign decision is here because we need to wait
	  		sign <= '0'; 				--for 1 cc to match with signExt_out signal
		else 							--otherwise sign decision is too early
			sign <= '1';
		end if;
	else
		--cw2(CW_SIZE-6 downto CW_SIZE-8) <= cw1(CW_SIZE - 6 downto CW_SIZE - 8);
		cw5 <= cw4(CW_SIZE -1 - 18 downto 0);
	end if;
    end if;
  end process CW_PIPE;

  -- purpose: Generation of ALU OpCode
  -- type   : combinational
  -- inputs : IR_i
  -- outputs: aluOpcode


  --- ALUOPCODE:
  --- [MSB] -> '1' if is a comparator op
  --- [MSB-1] -> '1' if is an unsigned op
  --- [MSB-2 downto 0] ALU/COMP internal OP

   ALU_OP_CODE_P : process (IR_opcode, IR_func)
   begin  -- process ALU_OP_CODE_P

	
	case to_integer(unsigned(IR_opcode)) is
	        -- case of R type requires analysis of FUNC
		when R_TYPE =>
			case to_integer(unsigned(IR_func)) is
				when SLL_RR_FUNC => aluOpcode_i <= ALU_SLL_A; 
				when SRL_RR_FUNC => aluOpcode_i <= ALU_SRL_A;  
				when SRA_RR_FUNC => aluOpcode_i <= ALU_SRA_A;  
				when ADD_RR_FUNC => aluOpcode_i <= ALU_ADD_A; 
				when ADDU_RR_FUNC => aluOpcode_i <= ALU_ADD_A; 
				when SUB_RR_FUNC => aluOpcode_i <= ALU_SUB_A;  
				when SUBU_RR_FUNC => aluOpcode_i <= ALU_SUB_A; 
				when AND_RR_FUNC => aluOpcode_i <= ALU_AND_A;  
				when OR_RR_FUNC => aluOpcode_i <= ALU_OR_A;  
				when XOR_RR_FUNC => aluOpcode_i <= ALU_XOR_A;
				when SEQ_RR_FUNC => aluOpcode_i <= ALU_SEQ_A; 
				when SNE_RR_FUNC => aluOpcode_i <= ALU_SNE_A;
				when SLT_RR_FUNC => aluOpcode_i <= ALU_SLT_A; 
				when SGT_RR_FUNC => aluOpcode_i <= ALU_SGT_A;  
				when SLE_RR_FUNC => aluOpcode_i <= ALU_SLE_A;  
				when SGE_RR_FUNC => aluOpcode_i <= ALU_SGE_A;  
				--when MOVI2S_RR_FUNC => aluOpcode_i <= LLS;  
				--when MOVS2I_RR_FUNC => aluOpcode_i <= LLS;  
				--when MOVF_RR_FUNC => aluOpcode_i <= LLS;  
				--when MOVD_RR_FUNC => aluOpcode_i <= LLS;  
				--when MOVFP2I_RR_FUNC => aluOpcode_i <= LLS; 
				--when MOVI2FP_RR_FUNC => aluOpcode_i <= LLS;  
				--when MOVI2T_RR_FUNC => aluOpcode_i <= LLS; 
				--when MOVT2I_RR_FUNC => aluOpcode_i <= LLS; 
				when SLTU_RR_FUNC => aluOpcode_i <= ALU_SLTU_A; 
				when SGTU_RR_FUNC => aluOpcode_i <= ALU_SGTU_A; 
				when SLEU_RR_FUNC => aluOpcode_i <= ALU_SLEU_A; 
				when SGEU_RR_FUNC => aluOpcode_i <= ALU_SGEU_A; 
				when others => aluOpcode_i <= ALU_AND_A;
			end case;

		when F_TYPE =>
			case to_integer(unsigned(IR_func)) is
				when MULTU_FUNC => aluOpcode_i <= MULTU_A;
				when MULT_FUNC => aluOpcode_i <= MULT_A;
				when others => aluOpcode_i <= ALU_AND_A;
			end case;

		when J_INSTR => aluOpcode_i <= ALU_ADD_A; -- j
		when JAL_INSTR => aluOpcode_i <= ALU_ADD_A; -- j
		when BEQZ_INSTR => aluOpcode_i <= ALU_ADD_A; -- j
		when BNEZ_INSTR => aluOpcode_i <= ALU_ADD_A; -- j
		when BFPT_INSTR => aluOpcode_i <= ALU_ADD_A; -- j
		when BFPF_INSTR => aluOpcode_i <= ALU_ADD_A; -- j
		when ADDI_INSTR => aluOpcode_i <= ALU_ADD_A; -- j
		when ADDUI_INSTR => aluOpcode_i <= ALU_ADD_A; -- j
		when SUBI_INSTR => aluOpcode_i <= ALU_SUB_A; -- j
		when SUBUI_INSTR => aluOpcode_i <= ALU_SUB_A; -- j
		when ANDI_INSTR => aluOpcode_i <= ALU_AND_A; -- j
		when ORI_INSTR => aluOpcode_i <= ALU_OR_A; -- j
		when XORI_INSTR => aluOpcode_i <= ALU_XOR_A; -- j
		when LHI_INSTR => aluOpcode_i <= ALU_ADD_A; -- j
		--when RFE_INSTR => aluOpcode_i <= ALU_SUB_A; -- j
		--when TRAP_INSTR => aluOpcode_i <= ALU_SUB_A; -- j
		when JR_INSTR => aluOpcode_i <= ALU_AND_A; -- j
		when JALR_INSTR => aluOpcode_i <= ALU_AND_A; -- j
		when SLLI_INSTR => aluOpcode_i <= ALU_SLL_A; -- j
		when NOP_INSTR => aluOpcode_i <= ALU_AND_A; -- j
		when SRLI_INSTR => aluOpcode_i <= ALU_SRL_A; -- j
		when SRAI_INSTR => aluOpcode_i <= ALU_SRA_A; -- j
		when SEQI_INSTR => aluOpcode_i <= ALU_SEQ_A; -- j
		when SNEI_INSTR => aluOpcode_i <= ALU_SNE_A; -- j
		when SLTI_INSTR => aluOpcode_i <= ALU_SLT_A; -- j
		when SGTI_INSTR => aluOpcode_i <= ALU_SGT_A; -- j
		when SLEI_INSTR => aluOpcode_i <= ALU_SLE_A; -- j
		when SGEI_INSTR => aluOpcode_i <= ALU_SGE_A; -- j
		when LB_TYPE => aluOpcode_i <= ALU_ADD_A; -- j
		when LH_TYPE => aluOpcode_i <= ALU_ADD_A; -- j
		when LW_INSTR => aluOpcode_i <= ALU_ADD_A; -- j
		when LBU_INSTR => aluOpcode_i <= ALU_ADD_A; -- j
		when LHU_INSTR => aluOpcode_i <= ALU_ADD_A; -- j
		when LF_INSTR => aluOpcode_i <= ALU_ADD_A; -- j
		when LD_INSTR => aluOpcode_i <= ALU_ADD_A; -- j
		when SB_INSTR => aluOpcode_i <= ALU_ADD_A; -- j
		when SH_INSTR => aluOpcode_i <= ALU_ADD_A; -- j
		when SW_INSTR => aluOpcode_i <= ALU_ADD_A; -- j
		when SF_INSTR => aluOpcode_i <= ALU_ADD_A; -- j
		when SD_INSTR => aluOpcode_i <= ALU_ADD_A; -- j
		--when ITLB_INSTR => aluOpcode_i <= ALU_SUB_A; -- j
		when SLTUI_INSTR => aluOpcode_i <= ALU_SLTU_A; -- j
		when SGTUI_INSTR => aluOpcode_i <= ALU_SGTU_A; -- j
		when SLEUI_INSTR => aluOpcode_i <= ALU_SLEU_A; -- j
		when SGEUI_INSTR => aluOpcode_i <= ALU_SGEU_A; -- j
		when others => aluOpcode_i <= ALU_AND_A;
	 end case;
	end process ALU_OP_CODE_P;

end dlx_cu_hw;
