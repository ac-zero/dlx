library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

package utils is

	--log2 function which is basically used to take logarithm base 2
	function log2  (data : integer  ) return integer;
	--increase is used to update cwp and swp to ensure a circular window structure
	function increase  (data : integer ; step: integer; max_value: integer ) return integer;
	--decrease is used to update cwp and swp to ensure a circular window structure
	function decrease  (data : integer ;step: integer;  max_value: integer ) return integer;
	--getIndex is used for the basic operations of the register file	
	function getIndex (cwp: std_logic_vector; address: std_logic_vector; max_value: integer) return integer;


	constant M: integer := 8;
	constant	N: integer := 8;
	constant	F: integer := 4;
	constant	numBit: integer := 64;
	
end utils;

package body utils is

	function log2 (data : integer  ) return integer is
		variable i : integer := 0;
	begin
		while 2**i < data and i < 31 loop
			i := i + 1;
		end loop;
 
		return i; 
	end log2;
	
	
	function increase  (data : integer ; step: integer; max_value: integer ) return integer is 
	begin
		return (data + step) mod max_value;
	end increase;
	
	function decrease  (data : integer ; step: integer; max_value: integer ) return integer is
		variable i: integer;
	begin
		i := data - step;
		if(i<0) then
			i := max_value - step;
		end if;
		return i;
	end decrease;
	
	function getIndex (cwp: std_logic_vector; address: std_logic_vector; max_value: integer) return integer is
		variable i,addr: integer;
	begin
		addr := to_integer(unsigned(address)) ;
		if ( addr < 3*N ) then
		-- sub regs
			i := (addr + to_integer(unsigned(cwp))) mod max_value;
		else
		-- globals regs
			i := max_value + addr-3*N; -- global regs start from max value (2*F*N)
		end if;
		
		return i;
	end getIndex;
	
end utils;
