library IEEE;
use IEEE.std_logic_1164.all; 

entity reg_gen_ddr is
	generic(NBIT: integer := 8);
	Port (	EN:	In	std_logic;
		D:	In	std_logic_vector(NBIT-1 downto 0);
		CK:	In	std_logic;
		RESET:	In	std_logic;
		Q:	Out	std_logic_vector(NBIT-1 downto 0));
end reg_gen_ddr;


architecture beh of reg_gen_ddr is
	
	signal data: std_logic_vector(NBIT-1 downto 0);
begin
	process(ck, RESET)
	begin
		if (RESET = '1') then
			data <= (others => '0');
		elsif ( EN = '1' ) then 
			if (ck'event) then
				data <= D;
			end if;
		end if;
	end process;

	Q <= data;

end beh;
