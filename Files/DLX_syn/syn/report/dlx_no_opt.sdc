###################################################################

# Created by write_sdc on Fri Oct 18 18:33:44 2019

###################################################################
set sdc_version 1.9

set_units -time ns -resistance MOhm -capacitance fF -voltage V -current mA
