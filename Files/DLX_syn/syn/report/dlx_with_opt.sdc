###################################################################

# Created by write_sdc on Fri Oct 18 18:35:39 2019

###################################################################
set sdc_version 1.9

set_units -time ns -resistance MOhm -capacitance fF -voltage V -current mA
create_clock [get_ports clk]  -period 2  -waveform {0 1}
set_max_delay 1  -from [list [get_ports clk] [get_ports rst] [get_ports {d_out_imem[31]}]      \
[get_ports {d_out_imem[30]}] [get_ports {d_out_imem[29]}] [get_ports           \
{d_out_imem[28]}] [get_ports {d_out_imem[27]}] [get_ports {d_out_imem[26]}]    \
[get_ports {d_out_imem[25]}] [get_ports {d_out_imem[24]}] [get_ports           \
{d_out_imem[23]}] [get_ports {d_out_imem[22]}] [get_ports {d_out_imem[21]}]    \
[get_ports {d_out_imem[20]}] [get_ports {d_out_imem[19]}] [get_ports           \
{d_out_imem[18]}] [get_ports {d_out_imem[17]}] [get_ports {d_out_imem[16]}]    \
[get_ports {d_out_imem[15]}] [get_ports {d_out_imem[14]}] [get_ports           \
{d_out_imem[13]}] [get_ports {d_out_imem[12]}] [get_ports {d_out_imem[11]}]    \
[get_ports {d_out_imem[10]}] [get_ports {d_out_imem[9]}] [get_ports            \
{d_out_imem[8]}] [get_ports {d_out_imem[7]}] [get_ports {d_out_imem[6]}]       \
[get_ports {d_out_imem[5]}] [get_ports {d_out_imem[4]}] [get_ports             \
{d_out_imem[3]}] [get_ports {d_out_imem[2]}] [get_ports {d_out_imem[1]}]       \
[get_ports {d_out_imem[0]}] [get_ports {d_out_dmem[31]}] [get_ports            \
{d_out_dmem[30]}] [get_ports {d_out_dmem[29]}] [get_ports {d_out_dmem[28]}]    \
[get_ports {d_out_dmem[27]}] [get_ports {d_out_dmem[26]}] [get_ports           \
{d_out_dmem[25]}] [get_ports {d_out_dmem[24]}] [get_ports {d_out_dmem[23]}]    \
[get_ports {d_out_dmem[22]}] [get_ports {d_out_dmem[21]}] [get_ports           \
{d_out_dmem[20]}] [get_ports {d_out_dmem[19]}] [get_ports {d_out_dmem[18]}]    \
[get_ports {d_out_dmem[17]}] [get_ports {d_out_dmem[16]}] [get_ports           \
{d_out_dmem[15]}] [get_ports {d_out_dmem[14]}] [get_ports {d_out_dmem[13]}]    \
[get_ports {d_out_dmem[12]}] [get_ports {d_out_dmem[11]}] [get_ports           \
{d_out_dmem[10]}] [get_ports {d_out_dmem[9]}] [get_ports {d_out_dmem[8]}]      \
[get_ports {d_out_dmem[7]}] [get_ports {d_out_dmem[6]}] [get_ports             \
{d_out_dmem[5]}] [get_ports {d_out_dmem[4]}] [get_ports {d_out_dmem[3]}]       \
[get_ports {d_out_dmem[2]}] [get_ports {d_out_dmem[1]}] [get_ports             \
{d_out_dmem[0]}]]  -to [list [get_ports {addr_imem[7]}] [get_ports {addr_imem[6]}] [get_ports    \
{addr_imem[5]}] [get_ports {addr_imem[4]}] [get_ports {addr_imem[3]}]          \
[get_ports {addr_imem[2]}] [get_ports {addr_imem[1]}] [get_ports               \
{addr_imem[0]}] [get_ports rm_dmem] [get_ports wm_dmem] [get_ports en_dmem]    \
[get_ports {addr_dmem[31]}] [get_ports {addr_dmem[30]}] [get_ports             \
{addr_dmem[29]}] [get_ports {addr_dmem[28]}] [get_ports {addr_dmem[27]}]       \
[get_ports {addr_dmem[26]}] [get_ports {addr_dmem[25]}] [get_ports             \
{addr_dmem[24]}] [get_ports {addr_dmem[23]}] [get_ports {addr_dmem[22]}]       \
[get_ports {addr_dmem[21]}] [get_ports {addr_dmem[20]}] [get_ports             \
{addr_dmem[19]}] [get_ports {addr_dmem[18]}] [get_ports {addr_dmem[17]}]       \
[get_ports {addr_dmem[16]}] [get_ports {addr_dmem[15]}] [get_ports             \
{addr_dmem[14]}] [get_ports {addr_dmem[13]}] [get_ports {addr_dmem[12]}]       \
[get_ports {addr_dmem[11]}] [get_ports {addr_dmem[10]}] [get_ports             \
{addr_dmem[9]}] [get_ports {addr_dmem[8]}] [get_ports {addr_dmem[7]}]          \
[get_ports {addr_dmem[6]}] [get_ports {addr_dmem[5]}] [get_ports               \
{addr_dmem[4]}] [get_ports {addr_dmem[3]}] [get_ports {addr_dmem[2]}]          \
[get_ports {addr_dmem[1]}] [get_ports {addr_dmem[0]}] [get_ports               \
{d_in_dmem[31]}] [get_ports {d_in_dmem[30]}] [get_ports {d_in_dmem[29]}]       \
[get_ports {d_in_dmem[28]}] [get_ports {d_in_dmem[27]}] [get_ports             \
{d_in_dmem[26]}] [get_ports {d_in_dmem[25]}] [get_ports {d_in_dmem[24]}]       \
[get_ports {d_in_dmem[23]}] [get_ports {d_in_dmem[22]}] [get_ports             \
{d_in_dmem[21]}] [get_ports {d_in_dmem[20]}] [get_ports {d_in_dmem[19]}]       \
[get_ports {d_in_dmem[18]}] [get_ports {d_in_dmem[17]}] [get_ports             \
{d_in_dmem[16]}] [get_ports {d_in_dmem[15]}] [get_ports {d_in_dmem[14]}]       \
[get_ports {d_in_dmem[13]}] [get_ports {d_in_dmem[12]}] [get_ports             \
{d_in_dmem[11]}] [get_ports {d_in_dmem[10]}] [get_ports {d_in_dmem[9]}]        \
[get_ports {d_in_dmem[8]}] [get_ports {d_in_dmem[7]}] [get_ports               \
{d_in_dmem[6]}] [get_ports {d_in_dmem[5]}] [get_ports {d_in_dmem[4]}]          \
[get_ports {d_in_dmem[3]}] [get_ports {d_in_dmem[2]}] [get_ports               \
{d_in_dmem[1]}] [get_ports {d_in_dmem[0]}] [get_ports {dmemCW_dmem[2]}]        \
[get_ports {dmemCW_dmem[1]}] [get_ports {dmemCW_dmem[0]}]]
