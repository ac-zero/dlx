vlib work

# CONSTANTS
vcom -reportprogress 300 -work work ../000-constants.vhd
vcom -reportprogress 300 -work work ../000-utils.vhd
vcom -reportprogress 300 -work work ../a.b.c-imem.vhd
vcom -reportprogress 300 -work work ../a.b.c-dmem.vhd


vcom -reportprogress 300 -work work ../dlx_with_opt.vhdl

# TB

vcom -reportprogress 300 -work work ../TB_TOP_DLX.vhd

