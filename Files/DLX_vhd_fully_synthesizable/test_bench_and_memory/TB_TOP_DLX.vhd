library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.constants.all;

entity DLX_TestBench is
end DLX_TestBench;

architecture tb of DLX_TestBench is

	component DLX is
		port (
			-- Inputs
			CLK						: in std_logic;		-- Clock
			RST						: in std_logic;		-- Reset:Active-High
--syn (imem)
		addr_imem: out std_logic_vector(PC_size-1 downto 0);
		d_out_imem: in std_logic_vector(IR_size-1 downto 0);

		--syn (dmem)
		rm_dmem,wm_dmem,en_dmem: out std_logic;
		addr_dmem: out std_logic_vector(SYSTEM_DATA_SIZE-1 downto 0);
		d_in_dmem: out std_logic_vector(SYSTEM_DATA_SIZE-1 downto 0);
		dmemCW_dmem: out std_logic_vector(DMEM_CW_SIZE-1 downto 0);
		d_out_dmem: in std_logic_vector(SYSTEM_DATA_SIZE-1 downto 0));
		
	end component;

	component imem is
	generic(PC_size: integer := 8;
		IR_size: integer := 32);
	port(	reset: in std_logic;
		addr: in std_logic_vector(PC_size-1 downto 0);
		d_out: out std_logic_vector(IR_size-1 downto 0));
	end component imem;

	component dmem is
	generic(DMEM_SIZE: integer := DMEM_SIZE;
		SYSTEM_DATA_SIZE: integer := SYSTEM_DATA_SIZE;
		DMEM_CW_SIZE: integer := DMEM_CW_SIZE);
	port(
		rst,clk,rm,wm,en: in std_logic;
		addr: in std_logic_vector(SYSTEM_DATA_SIZE-1 downto 0);
		d_in: in std_logic_vector(SYSTEM_DATA_SIZE-1 downto 0);
		dmemCW: in std_logic_vector(DMEM_CW_SIZE-1 downto 0);
		d_out: out std_logic_vector(SYSTEM_DATA_SIZE-1 downto 0));
	end component dmem;


	signal CLK :				std_logic := '0';		-- Clock
	signal RST :				std_logic;		-- Reset:Active-Low
	signal addr_imem: std_logic_vector(PC_size-1 downto 0);
	signal d_out_imem: std_logic_vector(IR_size-1 downto 0);

		--syn (dmem)
	signal rm_dmem,wm_dmem,en_dmem: std_logic;
	signal addr_dmem:  std_logic_vector(SYSTEM_DATA_SIZE-1 downto 0);
	signal d_in_dmem:  std_logic_vector(SYSTEM_DATA_SIZE-1 downto 0);
	signal dmemCW_dmem: std_logic_vector(DMEM_CW_SIZE-1 downto 0);
	signal d_out_dmem:  std_logic_vector(SYSTEM_DATA_SIZE-1 downto 0);
		
begin
	

	uut: DLX 
		port map (
			clk => clk,
			rst => rst,
			rm_dmem => rm_dmem,
			wm_dmem => wm_dmem,
			en_dmem => en_dmem,
			addr_imem => addr_imem,
			d_out_imem => d_out_imem,
			d_in_dmem => d_in_dmem,
			dmemCW_dmem => dmemCW_dmem,
			d_out_dmem => d_out_dmem
		);

	IMEM_unit: imem generic map(PC_size, IR_size)
		port map (
			reset => rst,
			addr => addr_imem,
			d_out => d_out_imem

		);

	DMEM_unit: dmem 
		port map (
			rst => rst,
			clk => clk,
			rm => rm_dmem,
			wm => wm_dmem,
			en => en_dmem,
			addr => addr_dmem,
			d_in => d_in_dmem,
			dmemCW => dmemCW_dmem,
			d_out => d_out_dmem

		);

	Clk <= not Clk after 10 ns;
	Rst <= '1', '0' after 5 ns;
	
end tb;

