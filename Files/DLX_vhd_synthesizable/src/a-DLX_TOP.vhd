library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.constants.all;
use work.utils.all;

entity dlx is
	generic(I_size: integer := IR_SIZE;
		PC_size: integer := PC_SIZE);
	port (
		-- Inputs
		CLK						: in std_logic;		-- Clock
		RST						: in std_logic;		-- Reset:Active-High

		IRAM_ADDRESS		: out std_logic_vector(PC_size - 1 downto 0);
		IRAM_ISSUE		: out std_logic;
		IRAM_READY		: in std_logic;
		IRAM_DATA		: in std_logic_vector(I_size-1 downto 0);
		
		DRAM_ADDRESS		: out std_logic_vector(PC_size-1 downto 0);
		DRAM_ISSUE		: out std_logic;
		DRAM_READNOTWRITE	: out std_logic;
		DRAM_READY		: in std_logic;
		DRAM_DATA		: inout std_logic_vector(2*Data_size-1 downto 0)
	);
	
end dlx;

architecture Structural of dlx is
	component add4 is
		generic( PC_size: integer := 8);
		port(	A: in std_logic_vector(PC_size-1 downto 0);
			B: in std_logic_vector(PC_size-1 downto 0);
			Z: out std_logic_vector(PC_size-1 downto 0));
	end component;

	component reg_gen is
		generic(NBIT: integer := 8);
		Port (	EN:	In 	std_logic;
			D:	In	std_logic_vector(NBIT-1 downto 0);
			CK:	In	std_logic;
			RESET:	In	std_logic;
			Q:	Out	std_logic_vector(NBIT-1 downto 0));
	end component;
	
   component dlx_cu is
		generic (
			MICROCODE_MEM_SIZE :     integer := MICROCODE_MEM_SIZE;  -- Microcode Memory Size
			FUNC_SIZE          :     integer := FUNC_SIZE;  -- Func Field Size for R-Type Ops
			OP_CODE_SIZE       :     integer := OP_CODE_SIZE;  -- Op Code Size
			ALU_OPC_SIZE     :     integer := ALU_CONTR_SIZE;  -- ALU Op Code Word Size
			IR_SIZE            :     integer := IR_SIZE;  -- Instruction Register Size    
			CW_SIZE            :     integer := CW_SIZE);  -- Control Word Size
		port (
			Clk                : in  std_logic;  -- Clock
			Rst                : in  std_logic;  -- Reset:Active-Low
			IR_IN              : in  std_logic_vector(IR_SIZE - 1 downto 0);
			IR_LATCH_EN        : out std_logic;  -- Instruction Register Latch Enable
			NPC_LATCH_EN       : out std_logic;
			RegA_LATCH_EN      : out std_logic;  -- Register A Latch Enable
			RegB_LATCH_EN      : out std_logic;  -- Register B Latch Enable
			RegIMM_LATCH_EN    : out std_logic;  -- Immediate Register Latch Enable
			MUXA_SEL           : out std_logic;  -- MUX-A Sel
			MUXB_SEL           : out std_logic;  -- MUX-B Sel
			ALU_OUTREG_EN      : out std_logic;  -- ALU Output Register Enable
			EQ_COND            : out std_logic;  -- Branch if (not) Equal to Zero
			ALU_OPCODE         : out std_logic_vector(ALU_OPC_SIZE-1 downto 0);
			DRAM_EN            : in std_logic;  -- Data RAM Write Enable
			DRAM_RD_WRN        : in std_logic;  -- Data RAM Write Enable
			LMD_LATCH_EN       : out std_logic;  -- LMD Register Latch Enable
			JUMP_EN            : out std_logic;  -- JUMP Enable Signal for PC input MUX
			PC_LATCH_EN        : out std_logic;  -- Program Counte Latch Enable
			WB_MUX_SEL         : out std_logic;  -- Write Back MUX Sel
			RF_WE              : out std_logic);  -- Register File Write Enable
	end component dlx_cu;
	
	component datapath is
		generic(
			IR_SIZE            :     integer := I_SIZE  -- Instruction Register Size    
		);
		port(
			Clk                : in  std_logic;  -- Clock
			Rst                : in  std_logic;  -- Reset:Active-Low
			IR_IN					 : in std_logic_vector(IR_SIZE-1 downto 0);
			NPC_IN				 : in std_logic_vector(PC_SIZE-1 downto 0);

			--IR_LATCH_EN        : in std_logic;  -- Instruction Register Latch Enable
			--NPC_LATCH_EN       : in std_logic; -- NextProgramCounter Register Latch Enable
			RegA_LATCH_EN      : in std_logic;  -- Register A Latch Enable
			RegB_LATCH_EN      : in std_logic;  -- Register B Latch Enable
			RegIMM_LATCH_EN    : in std_logic;  -- Immediate Register Latch Enable
			MUXA_SEL           : in std_logic;  -- MUX-A Sel
			MUXB_SEL           : in std_logic;  -- MUX-B Sel
			ALU_OUTREG_EN      : in std_logic;  -- ALU Output Register Enable
			EQ_COND            : in std_logic;  -- Branch if (not) Equal to Zero
			ALU_OPCODE         : in std_logic_vector(ALU_CONTR_SIZE-1 downto 0);
			DRAM_EN            : in std_logic;  -- Data RAM Write Enable
			DRAM_RD_WRN        : in std_logic;  -- Data RAM Write Enable
			LMD_LATCH_EN       : in std_logic;  -- LMD Register Latch Enable
			JUMP_EN            : in std_logic;  -- JUMP Enable Signal for PC input MUX
			--PC_LATCH_EN        : in std_logic;  -- Program Counte Latch Enable
			WB_MUX_SEL         : in std_logic;  -- Write Back MUX Sel
			RF_WE              : in std_logic;  -- Register File Write Enable
			
			PC_OUT				 : out std_logic_vector(PC_SIZE-1 downto 0)
		);
	end component datapath;

	signal IR_LATCH_EN        : std_logic;  -- Instruction Register Latch Enable
	signal NPC_LATCH_EN       : std_logic; -- NextProgramCounter Register Latch Enable
	signal RegA_LATCH_EN      : std_logic;  -- Register A Latch Enable
	signal RegB_LATCH_EN      : std_logic;  -- Register B Latch Enable
	signal RegIMM_LATCH_EN    : std_logic;  -- Immediate Register Latch Enable
	signal MUXA_SEL           : std_logic;  -- MUX-A Sel
	signal MUXB_SEL           : std_logic;  -- MUX-B Sel
	signal ALU_OUTREG_EN      : std_logic;  -- ALU Output Register Enable
	signal EQ_COND            : std_logic;  -- Branch if (not) Equal to Zero
	signal ALU_OPCODE         : std_logic_vector(ALU_CONTR_SIZE-1 downto 0);
	signal DRAM_EN            : std_logic;  -- Data RAM Write Enable
	signal DRAM_RD_WRN        : std_logic;  -- Data RAM Write Enable
	signal LMD_LATCH_EN       : std_logic;  -- LMD Register Latch Enable
	signal JUMP_EN            : std_logic;  -- JUMP Enable Signal for PC input MUX
	signal PC_LATCH_EN        : std_logic;  -- Program Counte Latch Enable
	signal WB_MUX_SEL         : std_logic;  -- Write Back MUX Sel
	signal RF_WE              : std_logic;  -- Register File Write Enable
	
	signal PC_OUT, PC_IN: std_logic_vector(PC_SIZE-1 downto 0);
	signal imem_dout, IR_OUT: std_logic_vector(I_size-1 downto 0);
	signal add_out, NPC_OUT: std_logic_vector(PC_size-1 downto 0);
	
	signal four: std_logic_vector(PC_size-1 downto 0) := std_logic_vector(to_unsigned(4,PC_SIZE));

begin

-- FETCH
	IRAM_ADDR <= PC_out;
	IRAM_ISSUE <= not(clk);

	PC_32: reg_gen
		generic map(NBIT => PC_SIZE)
		port map(EN => PC_LATCH_EN, D => PC_IN, CK => clk, RESET => rst, Q => PC_out);

	adder: add4
		generic map(PC_size => PC_size)
		port map(A => PC_out, B => four, Z => add_out);

	NPC_32: reg_gen
		generic map(NBIT => PC_SIZE)
		port map(EN => NPC_LATCH_EN, D => add_out, CK => clk, RESET => rst, Q => NPC_out);

	IR: reg_gen
		generic map(NBIT => I_SIZE)
		port map(EN => IR_LATCH_EN, D => IRAM_DATA, CK => clk, RESET => rst, Q => IR_out);
	
	dp: datapath generic map (I_SIZE)
		port map (
			clk => clk, 
			rst => rst, 
			IR_IN => IR_out, 
			NPC_IN => NPC_out, 
			RegA_LATCH_EN => RegA_LATCH_EN,
			RegB_LATCH_EN => RegB_LATCH_EN, 
			RegIMM_LATCH_EN => RegIMM_LATCH_EN,
			MUXA_SEL => MUXA_SEL, 
			MUXB_SEL => MUXB_SEL, 
			ALU_OUTREG_EN => ALU_OUTREG_EN,
			EQ_COND => EQ_COND, 
			ALU_OPCODE => ALU_OPCODE,
			LMD_LATCH_EN => LMD_LATCH_EN, 
			JUMP_EN => JUMP_EN,
			WB_MUX_SEL => WB_MUX_SEL, 
			RF_WE => RF_WE, 
			PC_OUT => PC_IN, 
			DRAM_EN => DRAM_EN, 
			DRAM_RD_WRN => DRAM_RD_WRN
		);
		
	
		cu: dlx_cu generic map (
			MICROCODE_MEM_SIZE => MICROCODE_MEM_SIZE,
			FUNC_SIZE => FUNC_SIZE,
			OP_CODE_SIZE => OP_CODE_SIZE,
			ALU_OPC_SIZE => ALU_CONTR_SIZE,   
			IR_SIZE => I_SIZE,
			CW_SIZE => CW_SIZE
		)
		port map (
			clk => clk, 
			rst => rst,
			IR_IN => IR_out,
			IR_LATCH_EN => IR_LATCH_EN, 
			NPC_LATCH_EN => NPC_LATCH_EN,
			RegA_LATCH_EN => RegA_LATCH_EN,
			RegB_LATCH_EN => RegB_LATCH_EN,
			RegIMM_LATCH_EN => RegIMM_LATCH_EN,
			MUXA_SEL => MUXA_SEL, 
			MUXB_SEL => MUXB_SEL, 
			ALU_OUTREG_EN => ALU_OUTREG_EN,
			EQ_COND => EQ_COND, 
			ALU_OPCODE => ALU_OPCODE, 
			LMD_LATCH_EN => LMD_LATCH_EN, 
			JUMP_EN => JUMP_EN, 
			PC_LATCH_EN => PC_LATCH_EN,
			WB_MUX_SEL => WB_MUX_SEL, 
			RF_WE => RF_WE, 
			DRAM_EN => DRAM_EN, 
			DRAM_RD_WRN => DRAM_RD_WRN
		);

		
end Structural;

























































