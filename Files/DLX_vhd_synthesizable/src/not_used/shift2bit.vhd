library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity shift2bit is
	port(	A: in std_logic_vector(31 downto 0);
		B: out std_logic_vector(31 downto 0));
end shift2bit;

architecture bhv of shift2bit is
begin
	B <= std_logic_vector(shift_left(unsigned(A), 2));
end bhv;
