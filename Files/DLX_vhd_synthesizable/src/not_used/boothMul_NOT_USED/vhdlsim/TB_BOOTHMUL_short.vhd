library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
--use WORK.constants.all;

entity MULTIPLIER_tb is
end MULTIPLIER_tb;


architecture TEST of MULTIPLIER_tb is


  constant N : integer := 32;    -- :=8  --:=16 
  constant numbit : integer := 32;    -- :=8  --:=16 
  constant resN: integer := 2*N;
  constant R : integer := 3;       

  --  input	 
  signal A_mp_i : std_logic_vector(numbit-1 downto 0) := (others => '0');
  signal B_mp_i : std_logic_vector(numbit-1 downto 0) := (others => '0');

  -- output
  signal Y_mp_i : std_logic_vector(2*numbit-1 downto 0);


	component BOOTHMUL is
		generic( N: integer := 8; R: integer := 3);
		port(
			A,B: in std_logic_vector(N-1 downto 0);
			S: out std_logic_vector(2*N-1 downto 0)
		);

	end component BOOTHMUL;

begin

	uut: BOOTHMUL
		generic map(N, R)
		port map (
			A => A_mp_i ,
			B => B_mp_i,
			S => Y_mp_i
		);

  test: process
  begin
	-- test mul with 0 as operand
	A_mp_i <= std_logic_vector(to_signed(0,numbit));
	B_mp_i <= std_logic_vector(to_signed(6495,numbit));
	wait for 10 ns;
	assert Y_mp_i = A_mp_i * B_mp_i report "Error on 0*B";
	A_mp_i <= std_logic_vector(to_signed(98365,numbit));
	B_mp_i <= std_logic_vector(to_signed(0,numbit));
	wait for 10 ns;
	assert Y_mp_i = A_mp_i * B_mp_i report "Error on A*0";
	
	-- test different sign mul
	A_mp_i <= (others => '1');
	B_mp_i <= std_logic_vector(to_signed(15632,numbit));
	wait for 10 ns;
	assert Y_mp_i = std_logic_vector(to_signed(-15632,resN)) report "Error on (-1)*B ";

	A_mp_i <= std_logic_vector(to_signed(1245,numbit));
	B_mp_i <=  (others => '1');
	wait for 10 ns;
	assert Y_mp_i = std_logic_vector(to_signed(-1245,resN)) report "Error on A*(-1) ";

	-- test all '1'
	A_mp_i <= (others => '1');
	B_mp_i <= (others => '1');
	wait for 10 ns;
	assert Y_mp_i = std_logic_vector(to_signed(1,numbit)) report "Error on all '1' ";

	-- test random values
	A_mp_i <= std_logic_vector(to_signed(1245,numbit));
	B_mp_i <= std_logic_vector(to_signed(15632,numbit));
	wait for 10 ns;
	assert Y_mp_i = A_mp_i * B_mp_i report "Error on random mul 1 ";

	A_mp_i <= std_logic_vector(to_signed(9375,numbit));
	B_mp_i <= std_logic_vector(to_signed(1245,numbit));
	wait for 10 ns;
	assert Y_mp_i = A_mp_i * B_mp_i report "Error on random mul 2 ";

	A_mp_i <= std_logic_vector(to_signed(-1245,numbit));
	B_mp_i <= std_logic_vector(to_signed(2947,numbit));
	wait for 10 ns;
	assert Y_mp_i = std_logic_vector(signed(A_mp_i) * signed(B_mp_i)) report "Error on random mul 3 ";

	A_mp_i <= std_logic_vector(to_signed(2153,numbit));
	B_mp_i <= std_logic_vector(to_signed(-1512,numbit));
	wait for 10 ns;
	assert Y_mp_i = std_logic_vector(signed(A_mp_i) * signed(B_mp_i)) report "Error on random mul 4 ";
  wait;          
  end process test;


end TEST;
