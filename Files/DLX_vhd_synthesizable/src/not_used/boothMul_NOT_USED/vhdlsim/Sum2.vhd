library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Sum2 is
	generic(N: integer := 4);
	port(
		A,B: in std_logic_vector(N-1 downto 0);
		Ci: in std_logic;
		S: out std_logic_vector(N-1 downto 0);
		Co: out std_logic
	);
end Sum2;

architecture Behavioral of Sum2 is
begin
	process(A,B,Ci)
		variable Sum: unsigned(N downto 0);
	begin
		Sum := unsigned('0'&A)+unsigned('0'&B)+('0'&Ci);
		S <= std_logic_vector(Sum(N-1 downto 0));
		Co <= std_logic(Sum(N)); 
	end process;

end Behavioral;

