package CONSTANTS is
   constant Nbit : integer := 32;
   constant R: integer := 3;
	constant ALU_CONTR_SIZE: integer := 3;
	constant DMEM_WORD_SIZE: integer := 8;
	constant DMEM_SIZE: integer := 8;
	
	constant IR_SIZE: integer:= 32;
	constant PC_SIZE: integer:= 8;
	constant CW_SIZE: integer:= 15;
	constant MICROCODE_MEM_SIZE:integer :=10;
	constant REG_SIZE: integer :=32;
	constant OP_CODE_SIZE: integer :=6;
	constant OPR_SIZE: integer :=5;
	constant IMM_SIZE: integer :=16;
	constant FUNC_SIZE: integer :=11;
	constant JUMP_SIZE: integer := 26;
end CONSTANTS;
