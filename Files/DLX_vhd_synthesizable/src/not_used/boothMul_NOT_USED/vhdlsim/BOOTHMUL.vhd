library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use WORK.constants.all;

entity BOOTHMUL is
	generic( N: integer := numbit; R: integer := radix);
	port(
		A,B: in std_logic_vector(N-1 downto 0);
		S: out std_logic_vector(2*N-1 downto 0)
	);

end BOOTHMUL;

architecture Structural of BOOTHMUL is
	
	-- booth encoder
	component BoothEnc is
		generic(R: integer:= 3; E: integer:= 3);
		port(
			A: in std_logic_vector(R-1 downto 0);
			O: out std_logic_vector(E-1 downto 0)
		);
	end component BoothEnc;
	
	-- N-bit 2 complement ( to obtain -A )
	component Comp2 is
		generic(N: integer:= 8);
		port(
			A: in std_logic_vector(N-1 downto 0);
			O: out std_logic_vector(N-1 downto 0)
		);
	end component Comp2;
	
	-- Left Shifter (used to duplicate value of A and -A)
	component LLS is
		generic(N: integer:= 8);
		port(
			A: in std_logic_vector(N-1 downto 0);
			O: out std_logic_vector(N-1 downto 0)
		);
	end component LLS;

	-- Simple behavioral adder
	component Sum2 is
		generic(N: integer := 4);
		port(
			A,B: in std_logic_vector(N-1 downto 0);
			Ci: in std_logic;
			S: out std_logic_vector(N-1 downto 0);
			Co: out std_logic
		);
	end component Sum2;
	
	component Mux8x1 is
		generic ( N: integer := 8);
		port(
			A,B,C,D,E,F,G,H: in std_logic_vector(N-1 downto 0);
			S: in std_logic_vector(2 downto 0);
			O: out std_logic_vector(N-1 downto 0)		
		);
	end component Mux8x1;
	
	constant M: integer := 2*N; -- N bit mul has 2*N bit output
	
	signal bn1: std_logic := '0';
	
	-- mux 'sel' signal array type
	subtype selST is std_logic_vector(R-1 downto 0);
	type selT is array(0 to N/2 - 2) of selST;
	signal sel: selT;
	
	-- mux output signal array type
	subtype muxoST is std_logic_vector(M-1 downto 0);
	type muxoT is array(0 to N/2 - 2) of muxoST;
	signal muxo: muxoT;
	
	-- mux inputs arrays
	signal Ai, Anegi, A2i, A2negi: muxoT;
	signal sumi: muxoT;
	
	signal enci: selT;	-- 
	signal pad: std_logic_vector(N-1 downto 0); -- padding vector
	
	-- signals for step 0  
	signal zero,A0: std_logic_vector(M-1 downto 0);
	signal A20,mux0,Aneg0, A2neg0: std_logic_vector(M-1 downto 0);
	signal sel0,benc0: std_logic_vector(R-1 downto 0);

	signal c: std_logic_vector(N/2 - 1 downto 0);
begin
	-- signals initialization
	bn1 <= '0';
	zero <= (others => '0');
	pad <= (others => A(N-1));
	A0 <= pad&A;
	benc0 <= B(1)&B(0)&bn1;
	
	stageGen: for i in 0 to N/2 - 2 generate
		-- first stage is unique since adder's inputs are 2 muxes
		s0: if i = 0 generate
			enc0: BoothEnc generic map (R,R) port map (benc0 , sel0);
			enc1: BoothEnc generic map (R,R) port map (B(3 downto 1), sel(0));
			mux1: Mux8x1 generic map (M) port map (zero, A0, Aneg0, A20, A2neg0, zero, zero, zero, sel0, mux0);
			mux2: Mux8x1 generic map (M) port map (zero, Ai(0), Anegi(0), A2i(0), A2negi(0), zero, zero, zero, sel(0), muxo(0));
			comp20: Comp2 generic map (M) port map (A0, Aneg0);
			LLS1: LLS generic map (M) port map (A0,A20);
			LLS2: LLS generic map (M) port map (Aneg0,A2neg0);
			LLS3: LLS generic map (M) port map (A20,Ai(0));
			LLS4: LLS generic map (M) port map (Ai(0),A2i(0));
			LLS5: LLS generic map (M) port map (A2neg0,Anegi(0));
			LLS6: LLS generic map (M) port map (Anegi(0),A2negi(0));
			sum0: Sum2 generic map (M) port map(mux0, muxo(0), bn1, sumi(0), c(0));
		end generate s0;

		-- other stages are generic, adder's inputs are a mux e previous adder's result
		s1: if i > 0 and i < N/2 - 1 generate
			enc0: BoothEnc generic map (R,R) port map (B(2*i+3 downto 2*i+1), sel(i));
			LLS1: LLS generic map (M) port map (A2i(i-1),Ai(i));
			LLS2: LLS generic map (M) port map (Ai(i),A2i(i));
			LLS3: LLS generic map (M) port map (A2negi(i-1),Anegi(i));
			LLS4: LLS generic map (M) port map (Anegi(i),A2negi(i));
			mux1: Mux8x1 generic map (M) port map (zero, Ai(i), Anegi(i), A2i(i), A2negi(i), zero, zero, zero, sel(i), muxo(i));
			sum0: Sum2 generic map (M) port map(sumi(i-1),muxo(i),bn1,sumi(i),c(i));
		end generate s1;
	end generate;
	
	S <= sumi(N/2-2);

end Structural;
