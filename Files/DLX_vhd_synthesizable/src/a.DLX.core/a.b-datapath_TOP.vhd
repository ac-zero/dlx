library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.constants.all;
use work.utils.all;


entity datapath is
	generic(
		IR_SIZE            :     integer := IR_SIZE -- Instruction Register Size    
	);
	port(
		Clk                : in  std_logic;  -- Clock
		Rst                : in  std_logic;  -- Reset:Active-Low
		
		IR_IN					 : in std_logic_vector(IR_SIZE-1 downto 0);
		NPC_IN				 : in std_logic_vector(PC_SIZE-1 downto 0);
		-- IF Control Signal
		--IR_LATCH_EN        : in std_logic;  -- Instruction Register Latch Enable
		--NPC_LATCH_EN       : in std_logic;
														-- NextProgramCounter Register Latch Enable
		-- ID Control Signals
		RegA_LATCH_EN      : in std_logic;  -- Register A Latch Enable
		RegB_LATCH_EN      : in std_logic;  -- Register B Latch Enable
		RegIMM_LATCH_EN    : in std_logic;  -- Immediate Register Latch Enable
	
		-- EX Control Signals
		MUXA_SEL           : in std_logic;  -- MUX-A Sel
		MUXB_SEL           : in std_logic;  -- MUX-B Sel
		ALU_OUTREG_EN      : in std_logic;  -- ALU Output Register Enable
		EQ_COND            : in std_logic;  -- Branch if (not) Equal to Zero
		-- ALU Operation Code
		ALU_OPCODE         : in std_logic_vector(ALU_CONTR_SIZE-1 downto 0);
		
		-- MEM Control Signals
		DRAM_EN            : in std_logic;  -- Data RAM Write Enable
		DRAM_RD_WRN        : in std_logic;  -- Data RAM Write Enable
		LMD_LATCH_EN       : in std_logic;  -- LMD Register Latch Enable
		JUMP_EN            : in std_logic;  -- JUMP Enable Signal for PC input MUX
		--PC_LATCH_EN        : in std_logic;  -- Program Counte Latch Enable
	
		-- WB Control signals
		WB_MUX_SEL         : in std_logic;  -- Write Back MUX Sel
		RF_WE              : in std_logic;  -- Register File Write Enable
		
		-- OUTPUTS
		PC_OUT				 : out std_logic_vector(PC_SIZE-1 downto 0)
		
	);
end datapath;

architecture Structural of datapath is

	component MUX21 is
		Port (	A:	In	std_logic;
			B:	In	std_logic;
			S:	In	std_logic;
			Y:	Out	std_logic);
	end component MUX21;

	component ALU is
		generic (
			N: integer := N;
			ALU_CONTR_SIZE: integer:= ALU_CONTR_SIZE
		);
		port(
			A,B: in std_logic_vector(N-1 downto 0);
			contr: in std_logic_vector(ALU_CONTR_SIZE-1 downto 0);
			S: out std_logic_vector(N-1 downto 0)
		);
	end component ALU;

	component zero_comp is
		generic(reg_size: integer := 32);
		port(	A: in std_logic_vector(reg_size-1 downto 0);
				zero: in std_logic;
			Z: out std_logic);
	end component;

	component reg_file is
		generic(reg_size: integer := 32);
		port(	enable: in std_logic;
			rd1_en: in std_logic;
			rd2_en: in std_logic;
			wr_en: in std_logic;
			rd1_addr: in std_logic_vector(log2(reg_size)-1 downto 0);
			rd2_addr: in std_logic_vector(log2(reg_size)-1 downto 0);
			wr_addr:  in std_logic_vector(log2(reg_size)-1 downto 0);
			data_in:  in std_logic_vector(reg_size-1 downto 0);
			data_out_1: out std_logic_vector(reg_size-1 downto 0);
			data_out_2: out std_logic_vector(reg_size-1 downto 0));
	end component;

	component reg_gen is
		generic(NBIT: integer := 8);
		Port (	EN:	In 	std_logic;
			D:	In	std_logic_vector(NBIT-1 downto 0);
			CK:	In	std_logic;
			RESET:	In	std_logic;
			Q:	Out	std_logic_vector(NBIT-1 downto 0));
	end component;

	component reg is
			Port (	EN:	In	std_logic;
			D:	In	std_logic;
			CK:	In	std_logic;
			RESET:	In	std_logic;
			Q:	Out	std_logic);
	end component;

	component sign_ext is
		port(	A : in std_logic_vector(15 downto 0);
			Z : out std_logic_vector(31 downto 0));
	end component;

	component mux21_gen is
		Generic (NBIT: integer:= 32);
		Port (	A:	In	std_logic_vector(NBIT-1 downto 0) ;
			B:	In	std_logic_vector(NBIT-1 downto 0);
			SEL:	In	std_logic;
			Y:	Out	std_logic_vector(NBIT-1 downto 0));
	end component;

	component dmem is
		generic(DMEM_SIZE: integer := DMEM_SIZE;
			DMEM_WORD_SIZE: integer := DMEM_WORD_SIZE);
		port(
			rm,wm,en: in std_logic;
			addr: in std_logic_vector(DMEM_SIZE-1 downto 0);
			d_in: in std_logic_vector(DMEM_WORD_SIZE-1 downto 0);
			d_out: out std_logic_vector(DMEM_WORD_SIZE-1 downto 0));
	end component dmem;

	signal PC: std_logic_vector(PC_size-1 downto 0) := (others => '0');
	signal imem_addr, signExt_out: std_logic_vector(IR_size-1 downto 0);
	signal RD1_en, RD2_en, WR_en, LMD_en : std_logic;
	signal data_out_1, data_out_2, data_in, aluReg_out, LMD_out, dMem_out: std_logic_vector(reg_size-1 downto 0);
	signal IMM_en,zComp_en, zComp_out,mux_JEN_out, aluReg_en : std_logic;
	signal imm_reg_out, data_A_out, data_B_out: std_logic_vector(IR_size-1 downto 0);
	signal cond_out: std_logic;
	signal RTYPEorITYPE_Rs2, RTYPEorITYPE_Rd:std_logic_vector(log2(reg_size)-1 downto 0);
	signal RTYPEorITYPE_IMM: std_logic_vector(imm_size-1 downto 0);

	-- ALU signals
	signal ALU_in_1, ALU_in_2: std_logic_vector(reg_size-1 downto 0);
	signal ALU_out: std_logic_vector(reg_size-1 downto 0); --to be deleted (ALU should be implemented)

	-- utils
	signal padding: std_logic_vector(IR_SIZE-PC_SIZE-1 downto 0);
	signal zero_sig: std_logic;

	begin

	RTYPEorITYPE_Rs2 <= IR_IN(IR_size-OP_CODE_SIZE-opr_size-1 downto func_size+opr_size) when IR_IN(IR_size-1 downto jump_size)="000000" else (others => 'Z');
	RTYPEorITYPE_Rd <= IR_IN(IR_size-OP_CODE_SIZE-opr_size*2-1 downto func_size) when IR_IN(IR_size-1 downto jump_size)="000000" else IR_IN(IR_size-OP_CODE_SIZE-opr_size-1 downto func_size+opr_size);
	RTYPEorITYPE_IMM <= (others => '0') when IR_IN(IR_size-1 downto jump_size)="000000" else IR_IN(IR_size-OP_CODE_SIZE-2*opr_size-1 downto 0);

	padding <= (others => '0');
	zero_sig <= '0';


-- DECODE

	RF: reg_file
		generic map(reg_size => reg_size)
		port map(enable => RF_WE, rd1_en => RD1_en, rd2_en => RD2_en, wr_en => WR_en,
			rd1_addr => IR_IN(IR_size-OP_CODE_SIZE-1 downto func_size+opr_size*2),
			rd2_addr => RTYPEorITYPE_Rs2,
			wr_addr => RTYPEorITYPE_Rd,
			data_in => data_in, data_out_1 => data_out_1, data_out_2 => data_out_2);

	signExt: sign_ext
		port map(A => RTYPEorITYPE_IMM, Z => signExt_out);

	IMM: reg_gen
		generic map(NBIT => IR_size)
		port map(EN => RegIMM_LATCH_EN, D => signExt_out, CK => clk, RESET => rst, Q => imm_reg_out);

	data_A: reg_gen
		generic map(NBIT => IR_size)
		port map(EN => RegA_LATCH_EN, D => data_out_1, CK => clk, RESET => rst, Q => data_A_out);

	data_B: reg_gen
		generic map(NBIT => IR_size)
		port map(EN => RegB_LATCH_EN, D => data_out_2, CK => clk, RESET => rst, Q => data_B_out);

	-- EXEC

	zeroComp: zero_comp
		generic map(reg_size => reg_size)
		port map(A => data_A_out,  zero => EQ_COND,Z => zComp_out);

	zCompReg: reg
		port map(EN => zComp_en, D => zComp_out, CK => clk, RESET => rst, Q => cond_out);

	mux_A: mux21_gen
		generic map(NBIT => reg_size)
		port map(A => padding&NPC_IN, B => data_A_out, SEL => MUXA_SEL, Y => ALU_in_1);

	mux_B: mux21_gen
		generic map(NBIT => reg_size)
		port map(A => data_B_out, B => imm_reg_out, SEL => MUXB_SEL, Y => ALU_in_2);

	alu_0: alu generic map (N => reg_size)
		port map (A => ALU_in_1, B => ALU_in_2, contr => ALU_OPCODE, S => ALU_out );

	aluReg: reg_gen
		generic map(NBIT => IR_size)
		port map(EN => ALU_OUTREG_EN, D => ALU_out, CK => clk, RESET => rst, Q => aluReg_out);

	-- MEM

	dmem_0: dmem generic map (DMEM_SIZE => DMEM_SIZE, DMEM_WORD_SIZE => DMEM_WORD_SIZE)
		port map(
			rm => DRAM_RD_WRN, wm => not(DRAM_RD_WRN), en => DRAM_EN, addr => ALU_out(DMEM_SIZE-1 downto 0),
			d_in => data_B_out(DMEM_WORD_SIZE-1 downto 0), d_out => dMem_out(DMEM_WORD_SIZE-1 downto 0));
		-- d_in is 8 bit, B is 32 bit !!!

	LMD: reg_gen
		generic map(NBIT => reg_size)
		port map(EN => LMD_LATCH_EN, D => dMem_out, CK => clk, RESET => rst, Q => LMD_out);

	mux_JEN: mux21
		port map(A => cond_out, B => zero_sig, S => JUMP_EN, Y => mux_JEN_out);
		
	mux_PC: mux21_gen
		generic map(NBIT => PC_SIZE)
		port map(A => NPC_IN, B => aluReg_out(PC_SIZE-1 downto 0), SEL => mux_JEN_out, Y => PC_OUT);	
		
	mux_WB: mux21_gen
		generic map(NBIT => reg_size)
		port map(A => LMD_out, B => aluReg_out, SEL => WB_MUX_SEL, Y => data_in);

end Structural;

