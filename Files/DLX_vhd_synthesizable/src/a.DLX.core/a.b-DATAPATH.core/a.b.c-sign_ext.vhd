library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.STD_LOGIC_unsigned.ALL;
--use IEEE.STD_LOGIC_arith.ALL;
use ieee.numeric_std.all;

entity sign_ext is
	port(	A : in std_logic_vector(15 downto 0);
		Z : out std_logic_vector(31 downto 0));
end sign_ext;

architecture bhv of sign_ext is
begin
	Z <= std_logic_vector(resize(signed(A), Z'length));
end bhv;
