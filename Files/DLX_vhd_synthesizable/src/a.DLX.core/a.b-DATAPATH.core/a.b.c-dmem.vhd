library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.constants.all;

entity dmem is
	generic(DMEM_SIZE: integer := DMEM_SIZE;
		DMEM_WORD_SIZE: integer := DMEM_WORD_SIZE);
	port(
		rm,wm,en: in std_logic;
		addr: in std_logic_vector(DMEM_SIZE-1 downto 0);
		d_in: in std_logic_vector(DMEM_WORD_SIZE-1 downto 0);
		d_out: out std_logic_vector(DMEM_WORD_SIZE-1 downto 0));
end dmem;

architecture bhv of dmem is

type memtype is array (0 to 2**DMEM_SIZE - 1) of std_logic_vector(DMEM_WORD_SIZE-1 downto 0);
signal mem : memtype;

begin

	memProc: process(rm,wm,en,addr,d_in)
	begin
		if(en = '1') then 
			if (rm = '1') then 
				d_out <= mem(to_integer(unsigned(addr)));
			elsif (wm = '1') then
				mem(to_integer(unsigned(addr))) <= d_in;
			end if;
		else
			d_out <= (others => '0');
		end if;
	end process;
	
end bhv;
