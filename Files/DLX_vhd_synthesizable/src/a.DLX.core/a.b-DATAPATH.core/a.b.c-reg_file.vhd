library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.utils.all;

entity reg_file is
	generic(reg_size: integer := 32);
	port(	enable: in std_logic;
		rd1_en: in std_logic;
		rd2_en: in std_logic;
		wr_en: in std_logic;
		rd1_addr: in std_logic_vector(log2(reg_size)-1 downto 0);
		rd2_addr: in std_logic_vector(log2(reg_size)-1 downto 0);
		wr_addr:  in std_logic_vector(log2(reg_size)-1 downto 0);
		data_in:  in std_logic_vector(reg_size-1 downto 0);
		data_out_1: out std_logic_vector(reg_size-1 downto 0);
		data_out_2: out std_logic_vector(reg_size-1 downto 0));
end reg_file;

architecture bhv of reg_file is

type regtype is array (0 to reg_size-1) of std_logic_vector(reg_size-1 downto 0);
signal rf : regtype;

begin

process(enable, rd1_en, rd2_en, wr_en, rd1_addr, rd2_addr, wr_addr, data_in) is
begin
rf(0) <= (others => '0'); --R0 always zero
if(enable='1') then
	if(rd1_en='1') then
		data_out_1 <= rf(to_integer(unsigned(rd1_addr)));
	end if;
	if(rd2_en='1') then
		data_out_2 <= rf(to_integer(unsigned(rd2_addr)));
	end if;
	if(wr_en='1') then
		rf(to_integer(unsigned(wr_addr))) <= data_in;
	end if;
else
	data_out_1 <= (others => 'Z');
	data_out_2 <= (others => 'Z');
end if;
end process;

end bhv;



