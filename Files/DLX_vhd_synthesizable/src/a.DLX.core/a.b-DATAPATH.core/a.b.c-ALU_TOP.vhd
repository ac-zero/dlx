library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.constants.all;

entity ALU is
	generic (
		N: integer := Nbit;
		ALU_CONTR_SIZE: integer:= ALU_CONTR_SIZE
	);
	port(
		A,B: in std_logic_vector(N-1 downto 0);
		contr: in std_logic_vector(ALU_CONTR_SIZE-1 downto 0);
		S: out std_logic_vector(N-1 downto 0)
	);
end ALU;

architecture Hybrid of ALU is
	
	component SHIFTER_GENERIC is
		generic(N: integer);
		port(	A: in std_logic_vector(N-1 downto 0);
			B: in std_logic_vector(4 downto 0);
			LOGIC_ARITH: in std_logic;	-- 1 = logic, 0 = arith
			LEFT_RIGHT: in std_logic;	-- 1 = left, 0 = right
			SHIFT_ROTATE: in std_logic;	-- 1 = shift, 0 = rotate
			OUTPUT: out std_logic_vector(N-1 downto 0)
		);
	end component SHIFTER_GENERIC;
	
	component pentium4adder is
		generic(N: integer := 32; R: integer := 4);
		port(
			A,B: in std_logic_vector(N-1 downto 0);
			Ci: in std_logic;
			S: out std_logic_vector(N-1 downto 0);
			Co: out std_logic
		);
	end component pentium4adder;
	
	component boolLogic is
		generic (N: integer := N);
		port(
			A,B: in std_logic_vector(N-1 downto 0);
			com: in std_logic_vector(1 downto 0);
			S: out std_logic_vector(N-1 downto 0)	
		);
	end component boolLogic;

	signal Cint,Co: std_logic;
	signal Sarith, Sshift, Sbool, Apadd, Bpadd: std_logic_vector(N-1 downto 0);
	signal cShift: std_logic_vector(2 downto 0);
	signal cBool: std_logic_vector(1 downto 0);
begin
	adder: pentium4adder generic map (N,4) port map (Apadd,Bpadd,Cint,Sarith,Co);
	shifter: shifter_generic generic map (N) port map (A,B(4 downto 0),cShift(0),
		cShift(1),cShift(2),Sshift);		
	bool: boolLogic generic map (N) port map (A,B,cBool,Sbool);
	
	outChooser: process(A,B,contr,Sarith,Sbool,Sshift)
	begin
		-- defaults 
		S <= (others => '0');
		cShift <= (others => '0');
		cBool <= (others => '0');
		Cint <= '0';
		
		
		case to_integer(unsigned(contr)) is
			when 0 => -- ADD
				Apadd <= A;
				Bpadd <= B;
				S <= Sarith;
				Cint <= '0';
			when 1 => -- SUB
				Apadd <= A;
				Bpadd <= Sbool;
				cBool <= "11";
				S <= Sarith;
				Cint <= '1';
			when 2 => -- AND
				S<= Sbool;
				cBool<= "00";
			when 3 => -- OR
				S<= Sbool;
				cBool<= "01";
			when 4 => -- XOR
				S<= Sbool;
				cBool<= "10";
			when 5 => -- SLL
				S<= Sshift;
				cShift<= "111";
			when 6 => -- SRL
				S<= Sshift;
				cShift<= "101";
			when 7 => -- SRA
				S<= Sshift;
				cShift<= "001";
			when others =>
				S <= (others => '0');
		end case;
	end process;
		
		
			


end Hybrid;

