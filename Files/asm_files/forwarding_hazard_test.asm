# hazards due to reg A
addui r1, r0, 1
addui r2, r1, 1		#data hazard (dep. on instr -1) (avoidable with forwarding)
addui r1, r0, 12	
addui r3, r2, 1		#data hazard (dep. on instr -2) (avoidable with forwarding)
addui r4, r2, 2		#no data hazard
sw 4(r0), r1 		
# hazards due to reg B
addui r1, r0, 1
add r2, r0, r1		#data hazard (dep. on instr -1) (avoidable with forwarding)
nop	
add r3, r1, r2		#data hazard (dep. on instr -2) (avoidable with forwarding)
add r4, r1, r2		#no data hazard
#load hazards
lw r5, 4(r0)
addui r6, r5, 1		#data hazard (dep. on instr -1) (NOT avoidable with forwarding)
lw r7, 4(r0)
nop
addui r8, r7, 8		#data hazard (dep. on instr -2) (avoidable with forwarding)
nop
#store hazards
lw r9, 4(r0)
sw 4(r9), r6 		#data hazard (dep. on instr -1) (NOT avoidable with forwarding)
sw 4(r9), r7	 	#data hazard (dep. on instr -2) ( avoidable with forwarding )
nop
#memory to memory forwarding
lw r10, 4(r20)
sw 20(r0), r10		#data hazard (dep on instr -1) ( avoidable with forwarding)
sw 24(r0), r10		#data hazard (dep on instr -2) ( avoidable with forwarding)
