addui r1, r0, 1
j label1
addi r17, r17, -1
addi r1, r0, -1		# if a negative value is written => branch failed
			# check R1, should be 1
label1:
bnez r2, label2
nop
addi r2, r0, 2
bnez r2, label2
addi r17, r17, -1
addui r2, r0, -2	# if a negative value is written => branch failed
			# check R2, should be 2
label2:
beqz r1, label3
nop
addi r3, r0, 3
beqz r4, label3
addi r17, r17, -1
addui r3, r0, -3	# if a negative value is written => branch failed
			# check R3, should be 3
label3:
addi r4, r0, 4
jal label4
addi r17, r17, -1
addi r4, r0, -4
			# check R4, should be 4
			# check R31, should be written
label4:
addi r5, r0, 5
addi r31, r31, 24
jr r31
addi r17, r17, -1
addi r5, r0, -5
			# check R5, should be 0
label5:
addi r6, r0, 6
jalr r0
addi r17, r17, -1
addi r6, r0, -6
			# check R31, should be changed
			# now the test should be restarted, otherwise it has failed
			
			# branch delay test: if branch delay is disabled then r17 should be 0
