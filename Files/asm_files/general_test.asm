#ALU test
addi r1, r0, 15 	# 15
addi r2, r0, -15 	# -15
addi r3, r0, 0 		# 0
subi r4, r1, 15 	# 0
subi r5, r1, -15 	# 30
subi r6, r1, 0 		# 15
addui r7, r2, 15	# 0
addui r8, r2, -15 	# 0xFFE2
addui r9, r2, 0 	# -15
subui r10, r3, 15 	# -15
subui r11, r3, -15 	# 15
subui r12, r3, 0 	# 0
add r13, r3, r1 	# 15
add r14, r3, r2 	# -15
add r15, r3, r3 	# 0
addu r16, r2, r4 	# -15
addu r17, r2, r5 	# 15
addu r18, r2, r6	# 0
sub r19, r1, r7 	# 15
sub r20, r1, r8 	# 
sub r21, r1, r9 	# 30
subu r22, r0, r10 	# 15
subu r23, r0, r11 	# -15
subu r24, r0, r12 	# 0

# Mult test
mult r4, r1, r2 # -225
mult r5, r2, r1 # -225
mult r6, r1, r3 # 0
mult r7, r2, r2 # 225
multu r8, r1, r2 # 0xEFFFFFF1F
multu r9, r2, r1 # 0xEFFFFFF1F
multu r10, r1, r3 # 0
multu r11, r2, r2 # 0xFFFFFFE2000000E1

addi r1, r0, 1
addi r3, r0, 3

# Booleans
andi r12, r2, 0xFFFFFFFF # -15
andi r13, r2, 0x00000000 # 0
ori r14, r2, 0x00000000 # -15
ori r15, r2, 0xFFFFFFFF # 0xFFFFFFFF
xori r16, r2, 0xFFFFFFFF # 14
xori r17, r2, 0x00000000 # -15
and r18, r2, r15 # -15
and r19, r2, r13 # 0
or r20, r2, r13 # -15
or r21, r2, r15 # 0xFFFFFFFF
xor r22, r2, r15 # 14
xor r23, r2, r13 # -15

# Shifts
slli r24, r1, 15 # 0x8000
srli r25, r2, 15 # 0x0001FFFF
srai r26, r2, 15 # 0xFFFFFFFF
sll r27, r1, r3 # 0x8
srl r28, r2, r3 # 0x1FFFFFFE
sra r29, r2, r3 # 0xFFFFFFFE
