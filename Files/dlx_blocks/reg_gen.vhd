library IEEE;
use IEEE.std_logic_1164.all; 

entity reg_gen is
	generic(NBIT: integer := 8);
	Port (	EN:	In	std_logic;
		D:	In	std_logic_vector(NBIT-1 downto 0);
		CK:	In	std_logic;
		RESET:	In	std_logic;
		Q:	Out	std_logic_vector(NBIT-1 downto 0));
end reg_gen;


architecture STRUCTURAL of reg_gen is

	component reg_latch is
	Port (	EN:	In	std_logic;
		D:	In	std_logic;
		CK:	In	std_logic;
		RESET:	In	std_logic;
		Q:	Out	std_logic);
	end component;

begin
	RF: for i in 0 to NBIT-1 generate
		regi: reg port map(EN,D(i),CK,RESET,Q(i));
		end generate;

end STRUCTURAL;
