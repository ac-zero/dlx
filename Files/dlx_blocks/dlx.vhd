library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.utils.all;

entity dlx is
	generic(I_size: integer := 32;
		PC_size: integer := 8);
	port(	clk: std_logic;
		rst: std_logic);
end dlx;

architecture bhv of dlx is

constant reg_size: integer :=32;
constant opc_size: integer :=8;
constant opr_size: integer :=5;
constant imm_size: integer :=16;
constant func_size: integer :=11;
constant jump_size: integer := 26;

--component dlx_cu (to be added)

--component ALU (to be added)

component zero_comp is
	generic(reg_size: integer := 32);
	port(	A: in std_logic_vector(reg_size-1 downto 0);
		Z: out std_logic);
end component;

component reg_file is
	generic(reg_size: integer := 32);
	port(	enable: in std_logic;
		rd1_en: in std_logic;
		rd2_en: in std_logic;
		wr_en: in std_logic;
		rd1_addr: in std_logic_vector(log2(reg_size)-1 downto 0);
		rd2_addr: in std_logic_vector(log2(reg_size)-1 downto 0);
		wr_addr:  in std_logic_vector(log2(reg_size)-1 downto 0);
		data_in:  in std_logic_vector(reg_size-1 downto 0);
		data_out_1: out std_logic_vector(reg_size-1 downto 0);
		data_out_2: out std_logic_vector(reg_size-1 downto 0));
end component;

component imem is
	generic(PC_size: integer := 8;
		I_size: integer := 32);
	port(	reset: in std_logic;
		addr: in std_logic_vector(I_size-1 downto 0);
		d_out: out std_logic_vector(I_size-1 downto 0));
end component;

component add4 is
	generic( PC_size: integer := 8);
	port(	A: in std_logic_vector(PC_size-1 downto 0);
		B: in std_logic_vector(PC_size-1 downto 0);
		Z: out std_logic_vector(PC_size-1 downto 0));
end component;

component reg_gen is
	generic(NBIT: integer := 8);
	Port (	EN:	In 	std_logic;
		D:	In	std_logic_vector(NBIT-1 downto 0);
		CK:	In	std_logic;
		RESET:	In	std_logic;
		Q:	Out	std_logic_vector(NBIT-1 downto 0));
end component;

component reg is
		Port (	EN:	In	std_logic;
		D:	In	std_logic;
		CK:	In	std_logic;
		RESET:	In	std_logic;
		Q:	Out	std_logic);
end component;

component sign_ext is
	port(	A : in std_logic_vector(15 downto 0);
		Z : out std_logic_vector(31 downto 0));
end component;

component mux21_gen is
	Generic (NBIT: integer:= 32);
	Port (	A:	In	std_logic_vector(NBIT-1 downto 0) ;
		B:	In	std_logic_vector(NBIT-1 downto 0);
		SEL:	In	std_logic;
		Y:	Out	std_logic_vector(NBIT-1 downto 0));
end component;

signal PC: std_logic_vector(PC_size-1 downto 0) := (others => '0');
signal imem_addr, imem_dout, signExt_out: std_logic_vector(I_size-1 downto 0);
signal add_out, IR_out: std_logic_vector(PC_size-1 downto 0);
signal four: std_logic_vector(PC_size-1 downto 0) := "00000100";
signal IR_en, NPC_en, PC_en, RF_en, RD1_en, RD2_en, WR_en, LMD_en : std_logic;
signal data_out_1, data_out_2, data_in, aluReg_out, NPC_out, PC_out, LMD_out, dMem_out: std_logic_vector(reg_size-1 downto 0);
signal IMM_en, dataA_en, dataB_en, zComp_en, zComp_out, aluReg_en : std_logic;
signal mux_2_2, mux_1_2, mux_2_1, mux_3_out: std_logic_vector(I_size-1 downto 0);
signal mux_4_s, mux_1_s, mux_2_s, mux_4_s: std_logic; --to be deleted (CU will generate them)
signal mux_3_s: std_logic;
signal ALU_out: std_logic_vector(reg_size-1 downto 0); --to be deleted (ALU should be implemented)
signal RTYPEorITYPE_Rs2, RTYPEorITYPE_Rd, RTYPEorITYPE_IMM: std_logic_vector(log2(reg_size)-1 downto 0);
signal ALU_in_1, ALU_in_2: std_logic_vector(reg_size-1 downto 0);

begin

RTYPEorITYPE_Rs2 <= IR_out(I_size-opc_size-opr_size-1 downto func_size+opr_size) when IR_out(I_size-1 downto jump_size)="000000" else (others => 'Z');
RTYPEorITYPE_Rd <= IR_out(I_size-opc_size-opr_size*2-1 downto func_size) when IR_out(I_size-1 downto jump_size)="000000" else IR_out(I_size-opc_size-opr_size-1 downto func_size+opr_size);
RTYPEorITYPE_IMM <= (others => '0') when IR_out(I_size-1 downto jump_size)="000000" else IR_out(I_size-opr_size*2-1 downto 0);

instr_mem: imem
	generic map(PC_size => PC_size, I_size => I_size)
	port map(reset => rst, addr => PC_out, d_out => imem_dout);

adder: add4
	generic map(PC_size => PC_size)
	port map(A => PC_out, B => four, Z => add_out);

PC_32: reg_gen
	generic map(NBIT => reg_size)
	port map(EN => PC_en, D => mux_3_out, CK => clk, RESET => rst, Q => PC_out);

NPC_32: reg_gen
	generic map(NBIT => reg_size)
	port map(EN => NPC_en, D => add_out, CK => clk, RESET => rst, Q => NPC_out);

IR: reg_gen
	generic map(NBIT => PC_size)
	port map(EN => IR_en, D => imem_dout, CK => clk, RESET => rst, Q => IR_out);

IMM: reg_gen
	generic map(NBIT => I_size)
	port map(EN => IMM_en, D => signExt_out, CK => clk, RESET => rst, Q => mux_2_2);

data_A: reg_gen
	generic map(NBIT => I_size)
	port map(EN => dataA_en, D => data_out_1, CK => clk, RESET => rst, Q => mux_1_2);

data_B: reg_gen
	generic map(NBIT => I_size)
	port map(EN => dataB_en, D => data_out_2, CK => clk, RESET => rst, Q => mux_2_1);

zCompReg: reg
	port map(EN => zComp_en, D => zComp_out, CK => clk, RESET => rst, Q => mux_3_s);

RF: reg_file
	generic map(reg_size => reg_size)
	port map(enable => RF_en, rd1_en => RD1_en, rd2_en => RD2_en, wr_en => WR_en,
		rd1_addr => IR_out(I_size-opc_size-1 downto func_size+opr_size*2),
		rd2_addr => RTYPEorITYPE_Rs2,
		wr_addr => RTYPEorITYPE_Rd,
		data_in => data_in, data_out_1 => data_out_1, data_out_2 => data_out_2);

aluReg: reg_gen
	generic map(NBIT => I_size)
	port map(EN => aluReg_en, D => ALU_out, CK => clk, RESET => rst, Q => aluReg_out);

LMD: reg_gen
	generic map(NBIT => reg_size)
	port map(EN => LMD_en, D => dMem_out, CK => clk, RESET => rst, Q => LMD_out);

signExt: sign_ext
	port map(A => RTYPEorITYPE_IMM, Z => signExt_out);

zeroComp: zero_comp
	generic map(reg_size => reg_size)
	port map(A => mux_1_2, Z => zComp_out);

mux_1: mux21_gen
	generic map(NBIT => reg_size)
	port map(A => NPC_out, B => mux_1_2, SEL => mux_1_s, Y => ALU_in_1);

mux_2: mux21_gen
	generic map(NBIT => reg_size)
	port map(A => mux_2_1, B => mux_2_2, SEL => mux_2_s, Y => ALU_in_2);

mux_3: mux21_gen
	generic map(NBIT => reg_size)
	port map(A => NPC_out, B => aluReg_out, SEL => mux_3_s, Y => mux_3_out);

mux_4: mux21_gen
	generic map(NBIT => reg_size)
	port map(A => LMD_out, B => aluReg_out, SEL => mux_4_s, Y => data_in);

end bhv;

























































