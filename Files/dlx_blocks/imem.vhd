library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;

entity imem is
	generic(PC_size: integer := 8;
		I_size: integer := 32);
	port(	reset: in std_logic;
		addr: in std_logic_vector(I_size-1 downto 0);
		d_out: out std_logic_vector(I_size-1 downto 0));
end imem;

architecture bhv of imem is

type memtype is array (0 to 2**PC_size - 1) of std_logic_vector(7 downto 0);
signal i_mem : memtype;

begin

d_out <= std_logic_vector(i_mem(to_integer(unsigned(addr)))) & std_logic_vector(i_mem(to_integer(unsigned(addr+1)))) &
	 std_logic_vector(i_mem(to_integer(unsigned(addr+2)))) & std_logic_vector(i_mem(to_integer(unsigned(addr+3))));

FILL_MEM_P: process(reset)
    file mem_fp: text;
    constant filename: string :="/home/servan/Desktop/imem.txt"; --can be changed
    variable file_line : line;
    variable i : integer := 0;
    variable temp : std_logic_vector(PC_size-1 downto 0);
begin 
    if (reset = '0') then
      file_open(mem_fp,filename,READ_MODE);
      while (not endfile(mem_fp)) loop
        readline(mem_fp,file_line);
		  next when file_line(1)='#';
        read(file_line,temp);
        i_mem(i) <= temp;     
        i := i + 1;
      end loop;
    end if;
end process FILL_MEM_P;

end bhv;
