# START SIMULATION

vsim work.dlx_tb -t ns

add wave -group DP sim:/dlx_tb/uut/dp/*
add wave -group CU sim:/dlx_tb/uut/cu/*
add wave -group RF sim:/dlx_tb/uut/dp/RF/*
add wave -group DMEM sim:/dlx_tb/uut/dp/dmem_0/*
add wave -group CU -group HDU sim:/dlx_tb/uut/cu/hazard_det_unit/*
add wave -group CU -group FU sim:/dlx_tb/uut/cu/fwu_gen_1/forwarding_unit/*


add wave sim:/dlx_tb/clk sim:/dlx_tb/rst
run 1000 ns
