vlib work

# CONSTANTS
vcom -reportprogress 300 -work work ../src/000-constants.vhd
vcom -reportprogress 300 -work work ../src/000-utils.vhd

# GENERIC COMPONENTS
vcom -reportprogress 300 -work work ../src/generics/01-iv.vhd
vcom -reportprogress 300 -work work ../src/generics/01-nd2.vhd
vcom -reportprogress 300 -work work ../src/generics/02-fa.vhd
vcom -reportprogress 300 -work work ../src/generics/03-mux21.vhd
vcom -reportprogress 300 -work work ../src/generics/03-FF.vhd
vcom -reportprogress 300 -work work ../src/generics/03-FF_ddr.vhd
vcom -reportprogress 300 -work work ../src/generics/04-mux21_gen.vhd
vcom -reportprogress 300 -work work ../src/generics/04-mux21_generic.vhd
vcom -reportprogress 300 -work work ../src/generics/04-rca_generic.vhd
vcom -reportprogress 300 -work work ../src/generics/04-reg_gen.vhd
vcom -reportprogress 300 -work work ../src/generics/04-reg_gen_ddr.vhd
vcom -reportprogress 300 -work work ../src/generics/05-generic_shifter.vhd
vcom -reportprogress 300 -work work ../src/generics/06-adder_gen.vhd

# PENTIUM 4 ADDER
vcom -reportprogress 300 -work work ../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-ALU.core/a.b.c.d-pentium4adder.core/a.b.c.d.e.f.g-csb.vhd
vcom -reportprogress 300 -work work ../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-ALU.core/a.b.c.d-pentium4adder.core/a.b.c.d.e.f-Gfunc.vhd
vcom -reportprogress 300 -work work ../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-ALU.core/a.b.c.d-pentium4adder.core/a.b.c.d.e.f-PGfunc.vhd
vcom -reportprogress 300 -work work ../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-ALU.core/a.b.c.d-pentium4adder.core/a.b.c.d.e-carryGen.vhd
vcom -reportprogress 300 -work work ../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-ALU.core/a.b.c.d-pentium4adder.core/a.b.c.d.e-sumg.vhd

# ALU
vcom -reportprogress 300 -work work ../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-ALU.core/a.b.c.d-pentium4adder_TOP.vhd
vcom -reportprogress 300 -work work ../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-ALU.core/a.b.c.d-boolLogic.vhd

# MUL
vcom -reportprogress 300 -work work ../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-BOOTHMUL.core/a.b.c.d-BoothEnc.vhd
vcom -reportprogress 300 -work work ../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-BOOTHMUL.core/a.b.c.d-Comp2.vhd
vcom -reportprogress 300 -work work ../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-BOOTHMUL.core/a.b.c.d-LLS.vhd
vcom -reportprogress 300 -work work ../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-BOOTHMUL.core/a.b.c.d-Mux8x1.vhd
vcom -reportprogress 300 -work work ../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-BOOTHMUL.core/a.b.c.d-Sum2.vhd

# DATAPATH
vcom -reportprogress 300 -work work ../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-ALU_TOP.vhd
vcom -reportprogress 300 -work work ../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-BOOTHMUL.vhd
vcom -reportprogress 300 -work work ../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-dmem.vhd
vcom -reportprogress 300 -work work ../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-reg_file.vhd
vcom -reportprogress 300 -work work ../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-sign_ext.vhd
vcom -reportprogress 300 -work work ../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-jump_unit.vhd
vcom -reportprogress 300 -work work ../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-adder_comp.vhd
vcom -reportprogress 300 -work work ../src/a-DLX.core/a.b-DATAPATH.core/a.b.c-imem.vhd

# CU
vcom -reportprogress 300 -work work ../src/a-DLX.core/a.b-CU_HW.core/a.b.c-hazard_det.vhd
vcom -reportprogress 300 -work work ../src/a-DLX.core/a.b-CU_HW.core/a.b.c-forwarding.vhd

# DLX
vcom -reportprogress 300 -work work ../src/a-DLX.core/a.b-datapath_TOP.vhd
vcom -reportprogress 300 -work work ../src/a-DLX.core/a.b-CU_HW.vhd


vcom -reportprogress 300 -work work ../src/a-DLX_TOP.vhd

# TB

vcom -reportprogress 300 -work work ../src/TB/TB_DLX.vhd

