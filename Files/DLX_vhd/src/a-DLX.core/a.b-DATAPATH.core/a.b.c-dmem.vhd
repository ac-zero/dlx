library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.constants.all;
use work.utils.all;

entity dmem is
	generic(DMEM_SIZE: integer := DMEM_SIZE;
		SYSTEM_DATA_SIZE: integer := SYSTEM_DATA_SIZE;
		DMEM_CW_SIZE: integer := DMEM_CW_SIZE);
	port(
		rst,clk,rm,wm,en: in std_logic;
		addr: in std_logic_vector(SYSTEM_DATA_SIZE-1 downto 0);
		d_in: in std_logic_vector(SYSTEM_DATA_SIZE-1 downto 0);
		dmemCW: in std_logic_vector(DMEM_CW_SIZE-1 downto 0);
		d_out: out std_logic_vector(SYSTEM_DATA_SIZE-1 downto 0));
end dmem;

architecture bhv of dmem is

type memtype is array (0 to DMEM_SIZE-1) of std_logic_vector(DMEM_WORD_SIZE-1 downto 0);
signal mem : memtype;

begin

	memProc: process(clk,rst,en)

	variable intADDR: integer := 0;	

	begin
		if rst = '1' then
			for i in 0 to DMEM_SIZE-1 loop
				mem(i) <= (others=>'0');
			end loop;
			d_out <= (others => '0');
		
		elsif(en = '1' and clk ='0' and clk'event) then
			intADDR := to_integer(unsigned(addr(log2(DMEM_SIZE)-1 downto 0)));
 
			if (rm = '1') then --READ
				if dmemCW(DMEM_CW_SIZE-1 downto 1)="00" then	--HALF WORD
					d_out(DMEM_WORD_SIZE-1 downto 0) <= mem(intADDR+1);
					d_out(2*DMEM_WORD_SIZE-1 downto DMEM_WORD_SIZE) <= mem(intADDR);
					if (dmemCW(0)='0') then	--UNSIGNED
						d_out(SYSTEM_DATA_SIZE-1 downto 2*DMEM_WORD_SIZE) <= (others => '0');
					else	--SIGNED
						d_out(SYSTEM_DATA_SIZE-1 downto 2*DMEM_WORD_SIZE) <= (others => mem(intADDR)(DMEM_WORD_SIZE-1));
					end if;
				elsif dmemCW(DMEM_CW_SIZE-1 downto 1)="01" then	--BYTE
					d_out(DMEM_WORD_SIZE-1 downto 0) <= mem(intADDR);
					if (dmemCW(0)='0') then	--UNSIGNED
						d_out(SYSTEM_DATA_SIZE-1 downto DMEM_WORD_SIZE) <= (others => '0');
					else	--SIGNED
						d_out(SYSTEM_DATA_SIZE-1 downto DMEM_WORD_SIZE) <= (others => mem(intADDR)(DMEM_WORD_SIZE-1));
					end if;
				elsif dmemCW(DMEM_CW_SIZE-1 downto 1)="10" then	-- WORD
					d_out(DMEM_WORD_SIZE-1 downto 0) <= mem(intADDR+3);
					d_out(2*DMEM_WORD_SIZE-1 downto DMEM_WORD_SIZE) <= mem(intADDR+2);
					d_out(3*DMEM_WORD_SIZE-1 downto 2*DMEM_WORD_SIZE) <= mem(intADDR+1);
					d_out(4*DMEM_WORD_SIZE-1 downto 3*DMEM_WORD_SIZE) <= mem(intADDR);

				--DOUBLE is missing ( dmemCW(2 downto 1)="11" )

				end if;

			elsif (wm = '1') then
				if dmemCW(DMEM_CW_SIZE-1 downto 1)="00" then	--HALF WORD
					mem(intADDR+1) <= d_in(DMEM_WORD_SIZE-1 downto 0);
					mem(intADDR) <= d_in(2*DMEM_WORD_SIZE-1 downto DMEM_WORD_SIZE);
				elsif dmemCW(DMEM_CW_SIZE-1 downto 1)="01" then	--BYTE
					mem(intADDR) <= d_in(DMEM_WORD_SIZE-1 downto 0);
				elsif dmemCW(DMEM_CW_SIZE-1 downto 1)="10" then	--WORD
					mem(intADDR+3) <= d_in(DMEM_WORD_SIZE-1 downto 0);
					mem(intADDR+2) <= d_in(2*DMEM_WORD_SIZE-1 downto DMEM_WORD_SIZE);
					mem(intADDR+1) <= d_in(3*DMEM_WORD_SIZE-1 downto 2*DMEM_WORD_SIZE);
					mem(intADDR) <= d_in(4*DMEM_WORD_SIZE-1 downto 3*DMEM_WORD_SIZE);

				--DOUBLE is missing ( dmemCW(2 downto 1)="11" )
				end if;
			end if;
		else
			d_out <= (others => 'Z');
		end if;
	end process;
	
end bhv;
