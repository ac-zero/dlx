library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.utils.all;

entity carryGen is
		generic (N: integer := 32; R: integer := 4);
		port(
			A,B: in std_logic_vector(N-1 downto 0);
			Ci: in std_logic;
			Co_0: out std_logic;
			C: out std_logic_vector(N/R -1 downto 0)
		);
end carryGen;
	
architecture Structural of carryGen is
	component PGfunc is
		port(
			gik, pik: in std_logic;
			gikp, pikp: in std_logic;
			gij, pij: out std_logic
		);
	end component PGfunc;

	component Gfunc is
		port(
			gik, pik: in std_logic;
			gikp: in std_logic;
			gij: out std_logic
		);
	end component Gfunc;
	
	subtype gijST is std_logic_vector(N downto 0);
	type gijT is array (log2(N) downto 0) of gijST;
	signal gij,pij: gijT;
	
begin	

	Co_0 <= Ci;
	
	pij(0)(0) <= '0';

	pgNetGen: for i in 1 to N generate		
		gij(0)(i) <= A(i-1) and B(i-1);
		pij(0)(i) <= A(i-1) or B(i-1);		
	end generate pgNetGen; 
	gij(0)(0) <= gij(0)(1) or (pij(0)(1) and Ci);
	gGen0: Gfunc port map (
		gij(0)(2),pij(0)(2),
		gij(0)(0),
		gij(1)(2)
	);
		
	p0: for i in 2 to N/2 generate 
		pg0: PGfunc port map ( 
			gij(0)(2*i),pij(0)(2*i),
			gij(0)(2*i-1),pij(0)(2*i-1),
			gij(1)(2*i),pij(1)(2*i)
		);
	end generate p0;

	treeGenG: for i in 2 to log2(N) generate
		colGen: for j in 1 to N generate
			gGen: if 2**i <= N and (j <= 2**(i) and j > 2**(i-1))
						and (j mod R = 0) generate 
					g0: Gfunc port map (
						gij(i-1)(j),pij(i-1)(j),
						gij(i-1)(2**(i-1)),
						gij(i)(j)
					);
					C(j/R-1) <= gij(i)(j);
			end generate gGen;
		
			pgGen: if 2**i <= N and j > 2**i
						and (j mod 2**i = 0 ) generate 
				p0: PGfunc port map (
					gij(i-1)(j),pij(i-1)(j),
					gij(i-1)(j-2**(i-1)),pij(i-1)(j-2**(i-1)),
					gij(i)(j),pij(i)(j)
				);
				
				pgGenCloneCond: if i > R-2 generate
					pgGenClone: for k in 1 to 2**(i-R+2)-1 generate
						pgGenBlock: if k < 2**(i-R+1) generate
							p0: PGfunc port map (
								gij(i-1)(j-k*R),pij(i-1)(j-k*R),
								gij(i-1)(j-2**(i-1)),pij(i-1)(j-2**(i-1)),
								gij(i)(j-k*R),pij(i)(j-k*R)
							);
						end generate pgGenBlock;
						
						pgGenConn: if k >= 2**(i-R+1) generate
							gij(i)(j-k*R) <= gij(i-1)(j-k*R);
							pij(i)(j-k*R) <= pij(i-1)(j-k*R);
						end generate pgGenConn;
					end generate pgGenClone;
				end generate pgGenCloneCond;
			end generate pgGen;
		
		end generate colGen;
	end generate treeGenG;
	
end Structural;

