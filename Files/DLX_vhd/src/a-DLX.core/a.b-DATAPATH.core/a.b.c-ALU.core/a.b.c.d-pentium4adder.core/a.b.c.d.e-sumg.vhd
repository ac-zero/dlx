library ieee; 
use ieee.std_logic_1164.all; 
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use WORK.constants.all;

entity sumg is
	generic(N: integer := 32;
		R: integer := 4);
	port( 
		A: IN std_logic_vector(N-1 downto 0);
		B: IN std_logic_vector(N-1 downto 0);
		Ci: IN std_logic_vector(N/R-1 downto 0);
		Ci_0: IN std_logic;
		S: OUT std_logic_vector(N-1 downto 0);
		Co: OUT std_logic);
end sumg;

architecture STRUCTURAL of sumg is

component csb is
	port(   Cin:	IN	std_logic;
		DATA_A: IN	std_logic_vector(3 downto 0);
		DATA_B: IN	std_logic_vector(3 downto 0);
		S:	OUT	std_logic_vector(3 downto 0));
end component;

begin

CSB_0: csb
		port map(Ci_0, A(3 downto 0), B(3 downto 0), S(3 downto 0));

CSBgen: for I in 1 to N/R-1 generate
	CSBI: csb
		port map(Ci(I-1), A(4*I+3 downto 4*I), B(4*I+3 downto 4*I), S(4*I+3 downto 4*I));
	end generate;

Co <= Ci(N/R-1);

end STRUCTURAL;

configuration CFG_SUMG_STRUCTURAL of sumg is
for STRUCTURAL
	for CSBgen
		for all : csb
			use configuration WORK.CFG_CSB_STRUCTURAL;
		end for;
	end for;
end for;
end CFG_SUMG_STRUCTURAL;

 













