library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity LLS is
	generic(N: integer:= 8);
	port(
		A: in std_logic_vector(N-1 downto 0);
		O: out std_logic_vector(N-1 downto 0)
	);
end LLS;

architecture Behavioral of LLS is

begin
	O <= A(N-2 downto 0)&'0';
end Behavioral;

