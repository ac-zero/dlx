library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.constants.all;

entity forwarding is
	generic(I_size: integer := IR_SIZE);
	port(	clk,rst: in std_logic;
		IR_IN: in std_logic_vector(IR_SIZE-1 downto 0);
		stall: in std_logic;
		FW_EXE_MEMN_A: out std_logic;
		FW_EXE_MEMN_B: out std_logic;
		FW_EN_A: out std_logic;
		FW_EN_B: out std_logic;
		FW_EN_M: out std_logic
	);
end forwarding;

architecture Behavioral of forwarding is
	subtype reg_st is std_logic_vector(OPR_SIZE-1 downto 0); 
	type reg_t is array (0 to 2) of reg_st;	
	signal regD: reg_t;

	signal regSa, regSb: reg_st;

	subtype opc_st is std_logic_vector(OP_CODE_SIZE-1 downto 0); 
	type opc_t is array (0 to 2) of opc_st;	
	signal opcode: opc_t;

	subtype out_sig_exe_st is std_logic_vector(4 downto 0); 
	type out_sig_exe_t is array (0 to 1) of out_sig_exe_st;	
	signal out_sig_exe: out_sig_exe_t;

	signal out_sig_mem: std_logic_vector(2 downto 0);
		
	signal opcode_temp: std_logic_vector(OP_CODE_SIZE-1 downto 0);
	signal out_sig_exe_temp: std_logic_vector(4 downto 0);
	signal out_sig_mem_temp: std_logic;
begin

	opcode_temp <= IR_IN(IR_SIZE-1 downto IR_SIZE-OP_CODE_SIZE);		
	
	shifterProc: process(clk,rst)
	begin
		if rst = '1' then
			regD <= (others => (others => '0'));
			regSa <= (others => '0');
			regSb <= (others => '0');
			opcode <= (others => (others => '0'));
			out_sig_exe <= (others => (others => '0'));
			out_sig_mem <= (others => '0');
		elsif clk = '1' and clk'event then
			if stall = '0' then
				opcode(0) <= opcode_temp;
				out_sig_exe(0) <= out_sig_exe_temp;
				out_sig_mem(0) <= out_sig_mem_temp;
				case opcode_temp(OP_CODE_SIZE-1 downto 1) is 
					when "00000" => 
						regSa <= IR_IN(IR_SIZE-OP_CODE_SIZE-1 downto IR_SIZE-OP_CODE_SIZE-OPR_SIZE);
						regSb <= IR_IN(IR_SIZE-OP_CODE_SIZE-OPR_SIZE-1 downto IR_SIZE-OP_CODE_SIZE-2*OPR_SIZE);
						regD(0) <= IR_IN(IR_SIZE-OP_CODE_SIZE-2*OPR_SIZE-1 downto IR_SIZE-OP_CODE_SIZE-3*OPR_SIZE);

					when others => 
						regSa <= IR_IN(IR_SIZE-OP_CODE_SIZE-1 downto IR_SIZE-OP_CODE_SIZE-OPR_SIZE);
						regSb <= (others => '0');
						regD(0) <= IR_IN(IR_SIZE-OP_CODE_SIZE-OPR_SIZE-1 downto IR_SIZE-OP_CODE_SIZE-2*OPR_SIZE);
				end case;

				if IR_IN(IR_SIZE-1 downto IR_SIZE-OP_CODE_SIZE) = "000011" or
				   IR_IN(IR_SIZE-1 downto IR_SIZE-OP_CODE_SIZE) = "010011" then
				   -- JUMP AND LINK EXCEPTION
				   regD(0) <= "11111";
				end if;

				if IR_IN(IR_SIZE-1 downto IR_SIZE-3) = "101" then
				   -- STORE EXCEPTION (2 source reg, no dest reg)
				   regSa <= IR_IN(IR_SIZE-OP_CODE_SIZE-1 downto IR_SIZE-OP_CODE_SIZE-OPR_SIZE);		   	   regSb <= IR_IN(IR_SIZE-OP_CODE_SIZE-OPR_SIZE-1 downto IR_SIZE-OP_CODE_SIZE-2*OPR_SIZE);  
				   regD(0) <= (others => '0');
				end if;

				opcode(1) <= opcode(0);
				regD(1) <= regD(0);
				regD(2) <= regD(1);
				out_sig_exe(1) <= out_sig_exe(0);
				out_sig_mem(1) <= out_sig_mem(0);
				out_sig_mem(2) <= out_sig_mem(1);

			end if;
		end if;	
	
	end process shifterProc;

	-- MAP
	--EXE_MEMN_A => out_sig_exe(i)(0);
	--EXE_MEMN_B => out_sig_exe(i)(1);
	--FW_EN_A => out_sig_exe(i)(2);
	--FW_EN_B => out_sig_exe(i)(3);
	
	--FW_EN_M => out_sig_mem(i);

	detectProc:process(rst,opcode,regD,regSa,regSb)
	begin
		if rst = '1' then 
			-- RST
			out_sig_exe_temp <= (others => '0');			
			out_sig_mem_temp <= '0';
		elsif stall = '0' then			
			-- DEFAULT
			out_sig_mem_temp <= '0';			
			out_sig_exe_temp <= (others => '0');			

			-- REG A
			if regSa /= "00000"  then
				if regSa = regD(1) then
				-- Result produced in EXE stage of instr[i-1]
				-- * to EXE
					if opcode(1)(OP_CODE_SIZE-1 downto 3) /= "100" then 
						-- FROM EXE ( Instr[i-1] is not a load )
						out_sig_exe_temp(0) <= '1';
					end if;
					out_sig_exe_temp(2) <= '1';
				elsif regSa = regD(2) then
				-- Result produced in instr[i-2]
				-- * to EXE
					out_sig_exe_temp(0) <= '0';
					out_sig_exe_temp(2) <= '1';
				end if;
	
			end if;
	
			-- REG B
			if regSb /= "00000" then
				-- In store instr. Sb is used in MEM stage
				if regSb = regD(1)  then
				-- Result produced in EXE stage of instr[i-1]
					if opcode(0)(OP_CODE_SIZE-1 downto 3) = "101" then
						-- * to MEM
						out_sig_mem_temp <= '1';
					else
						-- * to EXE
						if opcode(1)(OP_CODE_SIZE-1 downto 3) /= "100" then 
							-- FROM EXE ( Instr[i-1] is not a load )
							out_sig_exe_temp(1) <= '1';
						end if;
						out_sig_exe_temp(3) <= '1';
					end if;
				elsif regSb = regD(2) then 
				-- Result produced in instr[i-2]
				-- * TO EXE ( works also for * to MEM)
					out_sig_exe_temp(1) <= '0';
					out_sig_exe_temp(3) <= '1';
				end if;
			end if;
		end if;
	end process;


	-- OUTPUTS
	FW_EXE_MEMN_A <= out_sig_exe(0)(0);
	FW_EXE_MEMN_B <= out_sig_exe(0)(1);
	FW_EN_A <= out_sig_exe(0)(2);
	FW_EN_B <= out_sig_exe(0)(3);

	FW_EN_M <= out_sig_mem(1);
end Behavioral;
