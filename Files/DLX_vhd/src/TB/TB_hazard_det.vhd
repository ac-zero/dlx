LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE work.constants.all;
use std.textio.all;
use ieee.std_logic_textio.all;
 
ENTITY hazard_det_tb IS
END hazard_det_tb;
 
ARCHITECTURE behavior OF hazard_det_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 	component hazard_det is 
		generic(I_size: integer := IR_SIZE;
			forwarding: std_logic := '0');
		port(	clk,rst: in std_logic;
			IR_IN: in std_logic_vector(IR_SIZE-1 downto 0);
			stall: out std_logic);
	end component;


 	signal clk,rst: std_logic := '0';
	signal IR_IN: std_logic_vector(IR_SIZE-1 downto 0):= (others => '0');
	signal stall: std_logic := '0';
	
	constant clk_period: time := 10 ns;
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: hazard_det		 
	GENERIC MAP(IR_SIZE, '1')
	PORT MAP (
          clk => clk,
          rst => rst,
	  IR_IN => IR_IN,
	  stall => stall
        );

   clk_proc: process
   begin
	clk <= '1';
	wait for clk_period/2;
	clk <= '0';
	wait for clk_period/2;
   end process;

  -- Stimulus process
   stim_proc: process
	file mem_fp: text;
	constant filename: string :="../hazard_test.txt"; --can be changed
	variable file_line : line;
	variable i : integer := 0;
	variable temp : std_logic_vector(IR_size-1 downto 0);
   begin
	rst <= '1';
	wait for clk_period*10;
	rst <= '0';
	wait for clk_period*2;


	file_open(mem_fp,filename,READ_MODE);
      	while (not endfile(mem_fp)) loop
        	readline(mem_fp,file_line);
        	hread(file_line,temp);

		IR_IN <= temp;
		wait for clk_period;
		while stall = '1' loop
			wait for clk_period;
		end loop;
	end loop;
	
	wait;	
   end process;

END;
