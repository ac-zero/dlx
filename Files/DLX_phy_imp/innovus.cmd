#######################################################
#                                                     
#  Innovus Command Logging File                     
#  Created on Fri Oct 18 19:39:26 2019                
#                                                     
#######################################################

#@(#)CDS: Innovus v17.11-s080_1 (64bit) 08/04/2017 11:13 (Linux 2.6.18-194.el5)
#@(#)CDS: NanoRoute 17.11-s080_1 NR170721-2155/17_11-UB (database version 2.30, 390.7.1) {superthreading v1.44}
#@(#)CDS: AAE 17.11-s034 (64bit) 08/04/2017 (Linux 2.6.18-194.el5)
#@(#)CDS: CTE 17.11-s053_1 () Aug  1 2017 23:31:41 ( )
#@(#)CDS: SYNTECH 17.11-s012_1 () Jul 21 2017 02:29:12 ( )
#@(#)CDS: CPE v17.11-s095
#@(#)CDS: IQRC/TQRC 16.1.1-s215 (64bit) Thu Jul  6 20:18:10 PDT 2017 (Linux 2.6.18-194.el5)

set_global _enable_mmmc_by_default_flow      $CTE::mmmc_default
suppressMessage ENCEXT-2799
getDrawView
loadWorkspace -name Physical
win
set defHierChar /
set delaycal_input_transition_delay 0.1ps
set fpIsMaxIoHeight 0
set init_gnd_net gnd
set init_mmmc_file Default.view
set init_oa_search_lib {}
set init_pwr_net vdd
set init_verilog dlx_with_opt.v
set init_lef_file /software/dk/nangate45/lef/NangateOpenCellLibrary.lef
init_design
getIoFlowFlag
setIoFlowFlag 0
floorPlan -coreMarginsBy die -site FreePDK45_38x28_10R_NP_162NW_34O -r 1.0 0.6 5.0 5.0 5.0 5.0
uiSetTool select
getIoFlowFlag
fit
set sprCreateIeRingOffset 1.0
set sprCreateIeRingThreshold 1.0
set sprCreateIeRingJogDistance 1.0
set sprCreateIeRingLayers {}
set sprCreateIeRingOffset 1.0
set sprCreateIeRingThreshold 1.0
set sprCreateIeRingJogDistance 1.0
set sprCreateIeRingLayers {}
set sprCreateIeStripeWidth 10.0
set sprCreateIeStripeThreshold 1.0
set sprCreateIeStripeWidth 10.0
set sprCreateIeStripeThreshold 1.0
set sprCreateIeRingOffset 1.0
set sprCreateIeRingThreshold 1.0
set sprCreateIeRingJogDistance 1.0
set sprCreateIeRingLayers {}
set sprCreateIeStripeWidth 10.0
set sprCreateIeStripeThreshold 1.0
setAddRingMode -ring_target default -extend_over_row 0 -ignore_rows 0 -avoid_short 0 -skip_crossing_trunks none -stacked_via_top_layer metal10 -stacked_via_bottom_layer metal1 -via_using_exact_crossover_size 1 -orthogonal_only true -skip_via_on_pin {  standardcell } -skip_via_on_wire_shape {  noshape }
addRing -nets {gnd vdd} -type core_rings -follow core -layer {top metal9 bottom metal9 left metal10 right metal10} -width {top 0.8 bottom 0.8 left 0.8 right 0.8} -spacing {top 0.8 bottom 0.8 left 0.8 right 0.8} -offset {top 0.8 bottom 0.8 left 0.8 right 0.8} -center 1 -extend_corner {} -threshold 0 -jog_distance 0 -snap_wire_center_to_grid None
set sprCreateIeRingOffset 1.0
set sprCreateIeRingThreshold 1.0
set sprCreateIeRingJogDistance 1.0
set sprCreateIeRingLayers {}
set sprCreateIeRingOffset 1.0
set sprCreateIeRingThreshold 1.0
set sprCreateIeRingJogDistance 1.0
set sprCreateIeRingLayers {}
set sprCreateIeStripeWidth 10.0
set sprCreateIeStripeThreshold 1.0
set sprCreateIeStripeWidth 10.0
set sprCreateIeStripeThreshold 1.0
set sprCreateIeRingOffset 1.0
set sprCreateIeRingThreshold 1.0
set sprCreateIeRingJogDistance 1.0
set sprCreateIeRingLayers {}
set sprCreateIeStripeWidth 10.0
set sprCreateIeStripeThreshold 1.0
setAddStripeMode -ignore_block_check false -break_at none -route_over_rows_only false -rows_without_stripes_only false -extend_to_closest_target none -stop_at_last_wire_for_area false -partial_set_thru_domain false -ignore_nondefault_domains false -trim_antenna_back_to_shape none -spacing_type edge_to_edge -spacing_from_block 0 -stripe_min_length 0 -stacked_via_top_layer metal10 -stacked_via_bottom_layer metal1 -via_using_exact_crossover_size false -split_vias false -orthogonal_only true -allow_jog { padcore_ring  block_ring }
addStripe -nets {gnd vdd} -layer metal10 -direction vertical -width 0.8 -spacing 0.8 -set_to_set_distance 20 -start_from left -start_offset 15 -switch_layer_over_obs false -max_same_layer_jog_length 2 -padcore_ring_top_layer_limit metal10 -padcore_ring_bottom_layer_limit metal1 -block_ring_top_layer_limit metal10 -block_ring_bottom_layer_limit metal1 -use_wire_group 0 -snap_wire_center_to_grid None -skip_via_on_pin {  standardcell } -skip_via_on_wire_shape {  noshape }
setSrouteMode -viaConnectToShape { noshape }
sroute -connect { blockPin padPin padRing corePin floatingStripe } -layerChangeRange { metal1(1) metal10(10) } -blockPinTarget { nearestTarget } -padPinPortConnect { allPort oneGeom } -padPinTarget { nearestTarget } -corePinTarget { firstAfterRowEnd } -floatingStripeTarget { blockring padring ring stripe ringpin blockpin followpin } -allowJogging 1 -crossoverViaLayerRange { metal1(1) metal10(10) } -nets { vdd gnd } -allowLayerChange 1 -blockPin useLef -targetViaLayerRange { metal1(1) metal10(10) }
setPlaceMode -prerouteAsObs {1 2 3 4 5 6 7 8}
setPlaceMode -fp false
placeDesign
getPinAssignMode -pinEditInBatch -quiet
setPinAssignMode -pinEditInBatch true
editPin -fixOverlap 1 -unit MICRON -spreadDirection clockwise -side Top -layer 1 -spreadType start -spacing 0.14 -start 0.0 0.0 -pin {clk rst}
setPinAssignMode -pinEditInBatch true
editPin -fixOverlap 1 -unit MICRON -spreadDirection clockwise -side Right -layer 1 -spreadType start -spacing 0.14 -start 0.0 0.0 -pin {en_dmem rm_dmem wm_dmem}
setPinAssignMode -pinEditInBatch true
editPin -use GROUND -fixOverlap 1 -spreadDirection clockwise -side Right -layer 1 -spreadType side -pin {{addr_dmem[0]} {addr_dmem[1]} {addr_dmem[2]} {addr_dmem[3]} {addr_dmem[4]} {addr_dmem[5]} {addr_dmem[6]} {addr_dmem[7]} {addr_dmem[8]} {addr_dmem[9]} {addr_dmem[10]} {addr_dmem[11]} {addr_dmem[12]} {addr_dmem[13]} {addr_dmem[14]} {addr_dmem[15]} {addr_dmem[16]} {addr_dmem[17]} {addr_dmem[18]} {addr_dmem[19]} {addr_dmem[20]} {addr_dmem[21]} {addr_dmem[22]} {addr_dmem[23]} {addr_dmem[24]} {addr_dmem[25]} {addr_dmem[26]} {addr_dmem[27]} {addr_dmem[28]} {addr_dmem[29]} {addr_dmem[30]} {addr_dmem[31]} {d_in_dmem[0]} {d_in_dmem[1]} {d_in_dmem[2]} {d_in_dmem[3]} {d_in_dmem[4]} {d_in_dmem[5]} {d_in_dmem[6]} {d_in_dmem[7]} {d_in_dmem[8]} {d_in_dmem[9]} {d_in_dmem[10]} {d_in_dmem[11]} {d_in_dmem[12]} {d_in_dmem[13]} {d_in_dmem[14]} {d_in_dmem[15]} {d_in_dmem[16]} {d_in_dmem[17]} {d_in_dmem[18]} {d_in_dmem[19]} {d_in_dmem[20]} {d_in_dmem[21]} {d_in_dmem[22]} {d_in_dmem[23]} {d_in_dmem[24]} {d_in_dmem[25]} {d_in_dmem[26]} {d_in_dmem[27]} {d_in_dmem[28]} {d_in_dmem[29]} {d_in_dmem[30]} {d_in_dmem[31]} {d_out_dmem[0]} {d_out_dmem[1]} {d_out_dmem[2]} {d_out_dmem[3]} {d_out_dmem[4]} {d_out_dmem[5]} {d_out_dmem[6]} {d_out_dmem[7]} {d_out_dmem[8]} {d_out_dmem[9]} {d_out_dmem[10]} {d_out_dmem[11]} {d_out_dmem[12]} {d_out_dmem[13]} {d_out_dmem[14]} {d_out_dmem[15]} {d_out_dmem[16]} {d_out_dmem[17]} {d_out_dmem[18]} {d_out_dmem[19]} {d_out_dmem[20]} {d_out_dmem[21]} {d_out_dmem[22]} {d_out_dmem[23]} {d_out_dmem[24]} {d_out_dmem[25]} {d_out_dmem[26]} {d_out_dmem[27]} {d_out_dmem[28]} {d_out_dmem[29]} {d_out_dmem[30]} {d_out_dmem[31]} {dmemCW_dmem[0]} {dmemCW_dmem[1]} {dmemCW_dmem[2]}}
setPinAssignMode -pinEditInBatch true
editPin -fixOverlap 1 -spreadDirection clockwise -side Left -layer 1 -spreadType side -pin {{addr_imem[0]} {addr_imem[1]} {addr_imem[2]} {addr_imem[3]} {addr_imem[4]} {addr_imem[5]} {addr_imem[6]} {addr_imem[7]} {d_out_imem[0]} {d_out_imem[1]} {d_out_imem[2]} {d_out_imem[3]} {d_out_imem[4]} {d_out_imem[5]} {d_out_imem[6]} {d_out_imem[7]} {d_out_imem[8]} {d_out_imem[9]} {d_out_imem[10]} {d_out_imem[11]} {d_out_imem[12]} {d_out_imem[13]} {d_out_imem[14]} {d_out_imem[15]} {d_out_imem[16]} {d_out_imem[17]} {d_out_imem[18]} {d_out_imem[19]} {d_out_imem[20]} {d_out_imem[21]} {d_out_imem[22]} {d_out_imem[23]} {d_out_imem[24]} {d_out_imem[25]} {d_out_imem[26]} {d_out_imem[27]} {d_out_imem[28]} {d_out_imem[29]} {d_out_imem[30]} {d_out_imem[31]}}
setPinAssignMode -pinEditInBatch true
editPin -pinWidth 0.07 -pinDepth 0.07 -fixOverlap 1 -spreadDirection clockwise -side Left -layer 1 -spreadType side -pin {{addr_imem[0]} {addr_imem[1]} {addr_imem[2]} {addr_imem[3]} {addr_imem[4]} {addr_imem[5]} {addr_imem[6]} {addr_imem[7]} {d_out_imem[0]} {d_out_imem[1]} {d_out_imem[2]} {d_out_imem[3]} {d_out_imem[4]} {d_out_imem[5]} {d_out_imem[6]} {d_out_imem[7]} {d_out_imem[8]} {d_out_imem[9]} {d_out_imem[10]} {d_out_imem[11]} {d_out_imem[12]} {d_out_imem[13]} {d_out_imem[14]} {d_out_imem[15]} {d_out_imem[16]} {d_out_imem[17]} {d_out_imem[18]} {d_out_imem[19]} {d_out_imem[20]} {d_out_imem[21]} {d_out_imem[22]} {d_out_imem[23]} {d_out_imem[24]} {d_out_imem[25]} {d_out_imem[26]} {d_out_imem[27]} {d_out_imem[28]} {d_out_imem[29]} {d_out_imem[30]} {d_out_imem[31]}}
setPinAssignMode -pinEditInBatch false
setOptMode -fixCap true -fixTran true -fixFanoutLoad false
optDesign -postCTS
optDesign -postCTS -hold
getFillerMode -quiet
addFiller -cell FILLCELL_X8 FILLCELL_X4 FILLCELL_X32 FILLCELL_X2 FILLCELL_X16 FILLCELL_X1 -prefix FILLER
setNanoRouteMode -quiet -timingEngine {}
setNanoRouteMode -quiet -routeWithSiPostRouteFix 0
setNanoRouteMode -quiet -drouteStartIteration default
setNanoRouteMode -quiet -routeTopRoutingLayer default
setNanoRouteMode -quiet -routeBottomRoutingLayer default
setNanoRouteMode -quiet -drouteEndIteration default
setNanoRouteMode -quiet -routeWithTimingDriven false
setNanoRouteMode -quiet -routeWithSiDriven false
routeDesign -globalDetail
setAnalysisMode -analysisType onChipVariation
setOptMode -fixCap true -fixTran true -fixFanoutLoad false
optDesign -postRoute
optDesign -postRoute -hold
saveDesign dlx_32_8_1_1
win
reset_parasitics
extractRC
rcOut -setload dlx_32_8_1_1.setload -rc_corner standard
rcOut -setres dlx_32_8_1_1.setres -rc_corner standard
rcOut -spf dlx_32_8_1_1.spf -rc_corner standard
rcOut -spef dlx_32_8_1_1.spef -rc_corner standard
redirect -quiet {set honorDomain [getAnalysisMode -honorClockDomains]} > /dev/null
timeDesign -postRoute -pathReports -drvReports -slackReports -numPaths 50 -prefix dlx_32_8_1_1_postRoute -outDir timingReports
redirect -quiet {set honorDomain [getAnalysisMode -honorClockDomains]} > /dev/null
timeDesign -postRoute -hold -pathReports -slackReports -numPaths 50 -prefix dlx_32_8_1_1_postRoute -outDir timingReports
get_time_unit
report_timing -machine_readable -max_paths 10000 -max_slack 0.75 -path_exceptions all > top.mtarpt
load_timing_debug_report -name default_report top.mtarpt
verifyConnectivity -type all -error 1000 -warning 50
setVerifyGeometryMode -area { 0 0 0 0 } -minWidth true -minSpacing true -minArea true -sameNet true -short true -overlap true -offRGrid false -offMGrid true -mergedMGridCheck true -minHole true -implantCheck true -minimumCut true -minStep true -viaEnclosure true -antenna false -insuffMetalOverlap true -pinInBlkg false -diffCellViol true -sameCellViol false -padFillerCellsOverlap true -routingBlkgPinOverlap true -routingCellBlkgOverlap true -regRoutingOnly false -stackedViasOnRegNet false -wireExt true -useNonDefaultSpacing false -maxWidth true -maxNonPrefLength -1 -error 1000
verifyGeometry
setVerifyGeometryMode -area { 0 0 0 0 }
reportGateCount -level 5 -limit 100 -outfile dlx_32_8_1_1.gateCount
saveNetlist dlx_32_8_1_1.v
all_hold_analysis_views 
all_setup_analysis_views 
write_sdf  -ideal_clock_network dlx_32_8_1_1.sdf
