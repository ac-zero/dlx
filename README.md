# DLX

#### HOW TO WRITE BINARY CODE INTO I_MEMORY

1. from inside "Files" folder:
> bash assembler.sh [ path .asm file ]

#### HOW TO COMPILE AND SIMULATE DLX PROJECT

1. Go inside /DLX_vhd/sim folder
2. Open modelsim ( vsim )
3. compile all files using the command:
> source compile_script.tcl
4. simulate:
> source sim_script.tcl

Note: waves are splitted into groups in order to improve browsing


#### POSSIBLE OPTIMIZATIONS
+ Implement an optimized shifter
+ Division

#### WHAT'S MISSING FOR BASIC VERSION:
+ Synthesis
+ Place & Route 
+ Report
+ Tests & Benchmarks

