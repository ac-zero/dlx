\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Overview}{1}
\contentsline {section}{\numberline {1.1}Top View}{1}
\contentsline {section}{\numberline {1.2}Features}{1}
\contentsline {chapter}{\numberline {2}Datapath}{3}
\contentsline {section}{\numberline {2.1}Fetch}{4}
\contentsline {section}{\numberline {2.2}Decode}{4}
\contentsline {subsection}{\numberline {2.2.1}Register File}{4}
\contentsline {subsection}{\numberline {2.2.2}Data forwarding (Optimization)}{4}
\contentsline {subsection}{\numberline {2.2.3}Earlier branch computation (Optimization)}{5}
\contentsline {subsection}{\numberline {2.2.4}Jump Unit (Optimization)}{5}
\contentsline {section}{\numberline {2.3}Execution}{6}
\contentsline {subsection}{\numberline {2.3.1}ALU}{6}
\contentsline {subsubsection}{Logic Unit}{6}
\contentsline {subsection}{\numberline {2.3.2}Multiplication Unit (Optimization)}{7}
\contentsline {subsection}{\numberline {2.3.3}Comparator}{8}
\contentsline {section}{\numberline {2.4}Memory and Write-Back}{8}
\contentsline {chapter}{\numberline {3}Control Unit}{10}
\contentsline {section}{\numberline {3.1}Top View}{10}
\contentsline {section}{\numberline {3.2}Control Word Generation}{10}
\contentsline {section}{\numberline {3.3}Hazard Detection}{11}
\contentsline {subsection}{\numberline {3.3.1}Detection Process}{11}
\contentsline {section}{\numberline {3.4}Forwarding}{11}
\contentsline {chapter}{\numberline {4}Testbenches}{12}
\contentsline {section}{\numberline {4.1}Test 1: Branches}{12}
\contentsline {subsection}{\numberline {4.1.1}Tests with Branch Delay Disabled}{13}
\contentsline {subsection}{\numberline {4.1.2}Tests with Branch Delay Enabled}{14}
\contentsline {section}{\numberline {4.2}Test 2: Forwarding and Hazards}{15}
\contentsline {subsection}{\numberline {4.2.1}Forwarding Disabled}{16}
\contentsline {subsection}{\numberline {4.2.2}Forwarding Enabled}{17}
\contentsline {section}{\numberline {4.3}Test 3: Comparator Test}{18}
\contentsline {section}{\numberline {4.4}Test 4: General Test}{20}
\contentsline {chapter}{\numberline {5}Synthesis}{24}
\contentsline {chapter}{\numberline {6}Layout}{27}
\contentsline {chapter}{\numberline {7}Conclusion}{28}
